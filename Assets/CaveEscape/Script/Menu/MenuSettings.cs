using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuSettings : MonoBehaviour
{
    public int qualityLvl;
    public Text qualityTxt;
    public string[] qualityName;
    // Start is called before the first frame update
    void Start()
    {
        if (PlayerPrefs.HasKey("QualityLvl"))
        {
        qualityLvl = PlayerPrefs.GetInt("QualityLvl");
        ChangeQuality(qualityLvl);
        }

    }

    public void ChangeQuality(int value)
    {
        if (QualitySettings.GetQualityLevel() != value)
        {
        QualitySettings.SetQualityLevel(value);
        PlayerPrefs.SetInt("QualityLvl", qualityLvl);
        print(QualitySettings.GetQualityLevel());
        }
        
        qualityTxt.text = qualityName[qualityLvl];
    }

    public void ToggleQuality(bool advance)
    {
        print("TOGGLE");
        if (advance)
        {
            if (qualityLvl + 1 > 2)
            {
                qualityLvl = 0;
            }

            else
            {
                qualityLvl++;
            }
        }

        else
        {
            if (qualityLvl - 1 < 0)
            {
                qualityLvl = 2;
            }

            else
            {
                qualityLvl--;
            }
        }

        ChangeQuality(qualityLvl);
    }
}
