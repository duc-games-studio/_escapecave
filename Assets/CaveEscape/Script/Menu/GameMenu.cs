﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameMenu : MonoBehaviour
{
    public int screenIndex;

    public Animator screenAnimator;

    public GameObject[] screens;

    public LvLPanel[] lvlPanels;

    public ArrowButton backButton;

    public LvLPanel tmpLvlPanel;

    public JewelPanel jewelPanel;

    StagePersistent tmpPersist;

    private void Start()
    {
        Time.timeScale = 1;
        AudioListener.pause = false;
        tmpPersist = FindObjectOfType<StagePersistent>();

        CannotSelectPanels();
        CannotGoBack();

        tmpPersist.ShowBeginScreen(true);
        //if (tmpPersist.firstStart)
        //{
        //    tmpPersist.LoadData();
        //    tmpPersist.firstStart = false;
        //}

        if (tmpPersist.completed == true)
        {
            

            // Faz o cálculo da pontuação
            tmpPersist.CalculateScore();
            tmpPersist.CheckNextLock();
            tmpPersist.WriteSaveValues();

            // Animação de Fade in para painel de pontuação
            FindObjectOfType<ConfirmPanel>().FadeInToScore();

            screenIndex = 1;

            screenAnimator.SetTrigger("Straight1");
        }

        //ShowStars(tmpPersist.completed, tmpPersist.stageIndex);

        UpdateScreens();
        GetScores();
        UpdateLocks();

        /*if (screenIndex == 0)
        {
            
        }

        else
        {
            UpdateLocks();
            GetScores();
        }*/

        jewelPanel.GetValues();
        jewelPanel.WriteValues();
        tmpPersist.SaveData();

    }

    public void ChangeIndex(int value)
    {
        screenIndex = value;

        screenAnimator.SetTrigger("Screen" + screenIndex);
    }

    public void UpdateScreens()
    {
        for (int i = 0; i < screens.Length; i++)
        {
            if (i != screenIndex)
            {
                screens[i].SetActive(false);
            }

            else
            {
                screens[i].SetActive(true);
            }
        }
    }

    public void TestLoad()
    {
        tmpPersist.LoadData();
    }

    public void TestSave()
    {
        tmpPersist.WriteSaveValues();
        tmpPersist.SaveData();
    }

    public void TransferPersistent(LvLPanel panel)
    {
        panel.tmpPersist = tmpPersist;
    }

    public void GetScores()
    {

        for (int i = 0; i < lvlPanels.Length; i++)
        {
            TransferPersistent(lvlPanels[i]);
            if (!lvlPanels[i].locked)
            {
                lvlPanels[i].GetScore();
                //lvlPanels[i].EnableStars();
            }
        }
    }

    public void UpdateLocks()
    {
        //StagePersistent tmpPersist = FindObjectOfType<StagePersistent>();
        int st = tmpPersist.completedStages;

        for (int i = 0; i < lvlPanels.Length; i++)
        {
            if (i > st)
            {
                lvlPanels[i].Lock(true);
            }

            else
            {
                if (!tmpPersist.unlockedNext && i == tmpPersist.stageIndex)
                {
                    lvlPanels[i].Lock(false);
                }
                
            }
        }
    }

    public void CannotSelectPanels()
    {
        for (int i = 0; i < lvlPanels.Length; i++)
        {
            lvlPanels[i].Selection(false);
        }
    }

    public void CanSelectPanels()
    {
        for (int i = 0; i < lvlPanels.Length; i++)
        {
            if (!lvlPanels[i].locked)
            {
                lvlPanels[i].Selection(true);
            }            
        }
    }

    public void CanGoBack()
    {
        backButton.RTarget(true);
    }

    public void CannotGoBack()
    {
        backButton.RTarget(false);
    }

    public void SetTmpLvlPanel(LvLPanel tmpPanel)
    {
        tmpLvlPanel = tmpPanel;
    }



    public void ShowStars(bool hideCurrent, int stageIndex = 0)
    {
        for (int i = 0; i < lvlPanels.Length; i++)
        {
            if (lvlPanels[i].stageIndex == stageIndex)
            {
                lvlPanels[i].EnableStars(hideCurrent);
            }

            else
            {
                lvlPanels[i].EnableStars();
            }
        }
    }

    public void EnableCurrentStars()
    {
        lvlPanels[tmpPersist.stageIndex - 1].EnableStars();
    }

    public void AnimateLock()
    {
        if (tmpPersist.unlockedNext)
        {
            lvlPanels[tmpPersist.stageIndex].AnimateUnlock();
            tmpPersist.unlockedNext = false;

            CanSelectPanels();
        }
        
    }
}
