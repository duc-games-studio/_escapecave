﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public class ConfirmPanel : MonoBehaviour
{
    public Animator confirmAnimator;

    public UnityEvent fadeOutEvent;

    public UnityEvent animatingEvent;

    public UnityEvent endAnimatingEvent;

    string lvlName;



    [Header("Painel de pontuação")]
    public ScorePanel scorePanel;


    public GameObject characterClickArea;

    public void ShowConfirm()
    {
        confirmAnimator.SetBool("Confirm", true);
    }

    public void HideConfirm()
    {
        confirmAnimator.SetBool("Score", false);
        confirmAnimator.SetBool("Confirm", false);
    }

    public void ConfirmFadeOut()
    {
        confirmAnimator.SetTrigger("FadeOut");
    }

    public void InvokeFadeOut()
    {
        fadeOutEvent.Invoke();
    }

    public void InvokeAnimating()
    {
        animatingEvent.Invoke();
    }

    public void InvokeEndAnimating()
    {
        endAnimatingEvent.Invoke();
    }

    public void SetLvlName(string name)
    {
        lvlName = name;
    }

    public void LoadLevel()
    {
        SceneManager.LoadScene(lvlName);
    }



    // SCORE

    public void FadeInToScore()
    {
        scorePanel.buttons.SetActive(false);
        confirmAnimator.SetBool("Score", true);
        confirmAnimator.SetTrigger("ScoreFadeIn");
    }

    public void ScoreCountage()
    {
        scorePanel.BeginContage(scorePanel.itemIndex);
    }


    // SCORE PANEL

    public void ShowScore()
    {
        confirmAnimator.SetBool("Score", true);
        scorePanel.ShowHighScore();
    }

    public void HideScore()
    {
        confirmAnimator.SetBool("Score", false);
    }

    public void ShowWorkInProgress()
    {
        confirmAnimator.SetBool("WorkInProgress", true);
    }

    public void HideWorkInProgress()
    {
        confirmAnimator.SetBool("WorkInProgress", false);
    }



    public void DisableChracterArea()
    {
        characterClickArea.SetActive(false);
    }

    public void EnableCharacterArea()
    {
        characterClickArea.SetActive(true);
    }
}
