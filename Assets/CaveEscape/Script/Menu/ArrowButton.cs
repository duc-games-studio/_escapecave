﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ArrowButton : MonoBehaviour
{
    Animator anim;

    Image imgComp;

    private void Start()
    {
        anim = GetComponent<Animator>();

        imgComp = GetComponent<Image>();
    }

    public void SelectButton()
    {
        anim.SetBool("Selected", true);     
    }

    public void UnselectButton()
    {
        anim.SetBool("Selected", false);
    }

    public void ClickOnButton()
    {
        anim.SetTrigger("Clicked");
    }

    public void RTarget(bool b)
    {
        imgComp.raycastTarget = b;
    }

}
