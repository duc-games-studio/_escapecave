using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StagesPanel : MonoBehaviour
{
    public Animator anim;

    public GameObject rightButton, leftButton; 
 
    public void EnableRightButton(bool value)
    {
        rightButton.SetActive(value);
    }

    public void EnableLeftButton(bool value)
    {
        leftButton.SetActive(value);
    }
}
