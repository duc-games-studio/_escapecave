﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Playables;

public class Cutscene : MonoBehaviour
{

    public string sceneToLoad;

    public AsyncOperation loadOperation;

    public PlayableDirector timeline;

    bool cutsceneIsOver;

    private void Start()
    {
        loadOperation = SceneManager.LoadSceneAsync(sceneToLoad);
        loadOperation.allowSceneActivation = false;
    }

    private void Update()
    {
        if (loadOperation.isDone)
        {
            print("Done!");
        }

        if (cutsceneIsOver)
        {
            loadOperation.allowSceneActivation = true;
        }


    }

    public void CutsceneIsOver()
    {
        cutsceneIsOver = true;
    }
}
