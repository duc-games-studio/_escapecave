﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Playables;
using UnityEngine.InputSystem;

public class CutsceneButton : MonoBehaviour
{
    Animator anim;

    public float timeToSkip;

    float percentage;

    bool loading;

    bool mouseOver;

    bool holdKey;

    public Slider skipSlider;


    public PlayableDirector timeline;

    public GameObject secondFader;

    void Start()
    {
        anim = GetComponent<Animator>();
    }

    void Update()
    {
        if (Application.platform == RuntimePlatform.Android)
        {
            if (mouseOver)
            {
                if (Touchscreen.current.IsPressed())
                //if (Input.touchCount > 0)
                {
                    loading = true;
                }

            }

            else
            {
                loading = false;
            }
        }
        
        else
        {
            if (Keyboard.current.spaceKey.isPressed)
            {
                holdKey = true;
                Selected(true);
                loading = true;
            }

            else if (Mouse.current.leftButton.isPressed && mouseOver)
            {
                loading = true;
            }

            else
            {
                loading = false;
                if (holdKey)
                {
                    holdKey = false;
                    Selected(false);
                }
            }
        }


        if (loading)
        {
            percentage += Time.deltaTime / timeToSkip;
        }

        else
        {
            percentage = 0;
        }

        skipSlider.value = percentage;

        if (percentage >= 1)
        {
            AudioListener.pause = true;
            GetComponent<AudioSource>().ignoreListenerPause = true;
            GetComponent<AudioSource>().Play();
            Out();
        }
    }

    public void Selected(bool value)
    {
        anim.SetBool("Select", value);
        
    }

    public void Out()
    {
        anim.SetTrigger("Out");
        SkipScene();
        enabled = false;
    }

    public void MouseOver(bool value)
    {
        mouseOver = value;
    }

    public void SkipScene()
    {
        //timeline.Pause();
        secondFader.SetActive(true);
    }
}
