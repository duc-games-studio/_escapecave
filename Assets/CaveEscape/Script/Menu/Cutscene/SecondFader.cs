﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class SecondFader : MonoBehaviour
{
    public UnityEvent fadeOutEvent;

    public void InvokeFadeOut()
    {
        fadeOutEvent.Invoke();
    }
}
