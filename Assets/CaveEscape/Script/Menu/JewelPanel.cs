﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class JewelPanel : MonoBehaviour
{
    int rubis;
    int diamonds;

    public Text rubyTxt;
    public Text diamondTxt;
    public StagePersistent tmpPersist;
    private void Start()
    {
        //GetValues();
        //WriteValues();
    }

    public void GetValues()
    {
        tmpPersist = FindObjectOfType<StagePersistent>();
        rubis = tmpPersist.savedRubies;
        diamonds = tmpPersist.savedDiamonds;

        //rubis += tmpPersist.saveData.rubyAmount;
        //diamonds += tmpPersist.saveData.diamondAmount;
        //rubis += PlayerPrefs.GetInt("Rubies");
        //diamonds += PlayerPrefs.GetInt("Diamonds");

        //SaveJewels();
    }

    //public void SaveJewels()
    //{
    //    PlayerPrefs.SetInt("Rubies", rubis);
    //    PlayerPrefs.SetInt("Diamonds", diamonds);
    //}

    public void WriteValues()
    {
        rubyTxt.text = rubis.ToString();
        diamondTxt.text = diamonds.ToString();

        if (rubis > 999999)
        {
            rubyTxt.text = "999999+";
        }

        if (diamonds > 999999)
        {
            diamondTxt.text = "999999+";
        }
    }

}
