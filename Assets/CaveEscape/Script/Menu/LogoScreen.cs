﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem;

public class LogoScreen : MonoBehaviour
{
    public UnityEvent pressStartEvent;

    PlayerControls controls;



    private void Update()
    {
        //if (Input.touchCount > 0)
        //{
        //    pressStartEvent.Invoke();
        //}
        if (Application.platform == RuntimePlatform.Android)
        {
            print("ANDROID");
            if (Touchscreen.current.primaryTouch.isInProgress)
                //if (Touchscreen.current.IsPressed())
            //if (Input.touchCount > 0)
            {
                pressStartEvent.Invoke();
            }
        }
        else
        {
            if (Keyboard.current.anyKey.isPressed || Mouse.current.press.wasPressedThisFrame)
            {
                pressStartEvent.Invoke();
            }
        }

    }

    public void EnableStart()
    {
        enabled = true;
    }

}
