﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lock : MonoBehaviour
{
    public AudioSource aSource;

    public void PlayAudio()
    {
        aSource.Play();
    }

    public void DisableLock()
    {
        this.gameObject.SetActive(false);
    }
}
