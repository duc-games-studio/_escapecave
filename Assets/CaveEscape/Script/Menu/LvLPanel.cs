﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class LvLPanel : MonoBehaviour
{
    public Animator anim;

    public bool locked;

    public bool doNotUnlock;

    public bool canSelect;

    public bool played;

    public GameObject numberTxt;

    public GameObject lockImage;

    public Animator[] starAnimator;

    public string lvlName;

    public int stageIndex;

    public bool showIntroduction;

    [Header("(Usado para cálculo) Tempo máximo para completar fase")]
    public int stageTime;

    public EventTrigger eTrigger;


    // Pontuação
    public int[] starScore;
    public StagePersistent.StageData stData;

    /*public int enabledStars;

    [Header("Pontuação salva")]
    public int finalScore;

    public int timeValue;
    public int timeScore;

    public int rubyAmount;
    public int rubyScore;

    public int diamondAmount;
    public int diamondScore;

    public int retryAmount;
    public int retryScore;*/

    public StagePersistent tmpPersist;

    private void Start()
    {
        //anim = GetComponent<Animator>();

        if (locked)
        {
            Lock(true);
        }

        //tmpPersist = FindObjectOfType<StagePersistent>();
    }


    public void SelectPanel()
    {
        if (canSelect)
        {
            anim.SetBool("Selected", true);
        }
    }

    public void UnselectPanel()
    {
        if (canSelect)
        {
            anim.SetBool("Selected", false);
        }
    }

    public void ClickOnPanel()
    {
        if (canSelect)
        {
            anim.SetTrigger("Clicked");
        }
    }

    public void Selection(bool value)
    {
        canSelect = value;
        GetComponent<Image>().raycastTarget = value;
    }

    public void CannotSelect()
    {
        UnselectPanel();
        FindObjectOfType<GameMenu>().CannotSelectPanels();
        FindObjectOfType<GameMenu>().CannotGoBack();

    }

    public void Lock(bool value, bool cancel = false)
    {
        if (cancel)
        {
            return;
        }

        locked = value;

        if (value || doNotUnlock)
        {
            canSelect = false;
            lockImage.SetActive(true);
            eTrigger.enabled = false;
        }

        else
        {
            canSelect = true;
            lockImage.SetActive(false);
            eTrigger.enabled = true;
        }

    }

    public void AnimateUnlock()
    {
        lockImage.GetComponent<Animator>().enabled = true;
        locked = false;
        Selection(true);
        eTrigger.enabled = true;
    }

    public void ShowConfirmPanel()
    {
        if (played)
        {
            FindObjectOfType<ConfirmPanel>().ShowScore();
        }

        else
        {
            FindObjectOfType<ConfirmPanel>().ShowConfirm();
        }
        
    }

    public void TransferLvlName()
    {
        FindObjectOfType<ConfirmPanel>().SetLvlName(lvlName);

        // índice de fase atual para "StagePersistent"

        tmpPersist.SetStageIndex(stageIndex);
        tmpPersist.SetStageTime(stageTime);
    }




    // PONTUAÇÃO

    public void GetScore()
    {

        stData = tmpPersist.stData[stageIndex - 1];
        //finalScore = PlayerPrefs.GetInt("Stage" + stageIndex + "Score");


        //timeValue = PlayerPrefs.GetInt("Stage" + stageIndex + "TimeValue");

        //timeScore = PlayerPrefs.GetInt("Stage" + stageIndex + "TimeScore");



        //rubyAmount = PlayerPrefs.GetInt("Stage" + stageIndex + "RubyAmount");

        //rubyScore = PlayerPrefs.GetInt("Stage" + stageIndex + "RubyScore");



        //diamondAmount = PlayerPrefs.GetInt("Stage" + stageIndex + "DiamondAmount");

        //diamondScore = PlayerPrefs.GetInt("Stage" + stageIndex + "DiamondScore");



        //retryAmount = PlayerPrefs.GetInt("Stage" + stageIndex + "RetryAmount");

        //retryScore = PlayerPrefs.GetInt("Stage" + stageIndex + "RetryScore");



        //enabledStars = GetStars(finalScore);



        if (stData.finalScore != 0)
        {
            played = true;
        }        



        if (stData.enabledStars > 0)
        {
            if (tmpPersist.completedStages < stageIndex)
            {
                tmpPersist.SetCompletedStages(stageIndex);
            }
        }

        if (tmpPersist.completed && tmpPersist.stageIndex == stageIndex)
        {
            return;
        }

        EnableStars();
        
    }


    public int GetStars(int finalScore)
    {

        int stars = 0;

        for (int i = 0; i < 3; i++)
        {
            if (finalScore >= starScore[i])
            {
                stars = i + 1;
                // print("Stars " + stars + " CÁLCULO NOJENTO");
            }
        }

        //print(starScore[0] + "Star Score1");
        //print(starScore[1] + "Star Score2");
        //print(starScore[2] + "Star Score3");
        //print(finalScore + "Score Final!");
        //print(tmpStar + " tmpStar");
        return stars;
    }


    [ContextMenu("EnableStars")]
    public void EnableStars(bool cancel = false)
    {

        if (cancel)
        {
            //for (int i = 0; i < 3; i++)
            //{
            //    starAnimator[i].enabled = false;
            //}
            return;
        }

        if (stData.enabledStars > 0)
        {
            for (int i = 0; i < stData.enabledStars; i++)
            {
                starAnimator[i].enabled = true;
                starAnimator[i].SetTrigger("On");
            }
        }
    }


    public void TemporaryPanel()
    {
        FindObjectOfType<GameMenu>().SetTmpLvlPanel(this);
    }
}
