﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.InputSystem;

public class ScorePanel : MonoBehaviour
{
    public float typeTime;

    [System.Serializable]
    public class ScoreItem
    {
        // Objeto no painel que mostra a pontuação
        public GameObject obj;
        // Texto que mostra o valor do item coletado
        public Text valueTxt;
        // a quantidade que o item representa
        // EX = 10 jóias azuis
        public int amountValue;
        // A pontuação gerada pela quantidade de itens
        public int pointValue;
        // Tipo de pontuação
        public ScoreType scoreType;
    }
    // Texto que fica no topo do painel
    public Text labelTxt;
    // Itens de pontuação
    public ScoreItem[] scoreItens;
    // Qual item está sendo usado no momento
    public int itemIndex;
    // Texto final
    public Text finalObsTxt;
    // Animador para as estrelas
    public Animator[] starAnimators;
    // Quantidade de es trelas
    public int starsAmount;
    // Botões (Retry e Advance)
    public GameObject buttons;
    // Script que calculará a pontuação
    StagePersistent stPersist;
    // Bools que indicam o que está sendo escrito no painel
    bool typingItems, typingStars;
    // Efeitos sonoros do painel
    public SFX menuSFX;
    // Painel de fase Temporário
    public LvLPanel tmpPanel;

    void Start()
    {
        stPersist = FindObjectOfType<StagePersistent>();        
        enabled = false;
    }

    public void Update()
    {
        if (typingItems || typingStars)
        {

            if (Application.platform == RuntimePlatform.Android)
            {
                if (Touchscreen.current.primaryTouch.isInProgress)
                {
                    ToNextItem();
                }
            }

            else
            {
                if (Keyboard.current.spaceKey.wasPressedThisFrame || Mouse.current.leftButton.wasPressedThisFrame)
                {
                    ToNextItem();
                }
                if (Keyboard.current.ctrlKey.isPressed)
                {
                    ToNextItem();
                }
            }

        }

        else
        {
            enabled = false;
        }
    }

    // Desabilita os itens do painel para começar a contagem
    public void DisableItens()
    {
        labelTxt.text = "RESULTS";
        for (int i = 0; i < scoreItens.Length; i++)
        {
            scoreItens[i].obj.SetActive(false);
        }

        finalObsTxt.gameObject.SetActive(false);
        buttons.SetActive(false);
    }

    // Pega os valores finais
    public void GetNumbers(bool tmpLvlPanel)
    {
        //Se "tmpLvlPanel" for verdadeiro, pega os valores do último painel selecionado no menu
        if (tmpLvlPanel)
        {
            tmpPanel = FindObjectOfType<GameMenu>().tmpLvlPanel;
        }
        //Se "tmpLvlPanel" for falso, pega os valores da última fase jogada
        else
        {
            tmpLvlPanel = FindObjectOfType<GameMenu>().lvlPanels[stPersist.stageIndex];
        }
        starsAmount = tmpPanel.stData.enabledStars;
        for (int i = 0; i < scoreItens.Length; i++)
        {
            switch (scoreItens[i].scoreType)
            {
                case ScoreType.Time:
                    {
                        scoreItens[i].amountValue = tmpPanel.stData.timeValue;
                        //scoreItens[i].pointValue = tmpPanel.stData.timeScore;
                    }
                    break;

                case ScoreType.Rubies:
                    {
                        scoreItens[i].amountValue = tmpPanel.stData.rubyAmount;
                        //scoreItens[i].pointValue = tmpPanel.stData.rubyScore;
                    }
                    break;

                case ScoreType.Diamonds:
                    {
                        scoreItens[i].amountValue = tmpPanel.stData.diamondAmount;
                        //scoreItens[i].pointValue = tmpPanel.stData.diamondScore;
                    }
                    break;
            }
        }
        // POR ALGUMA RAZÃO FOR NÃO FUNCIONA
        /*for (int i = 0; i < 3; i++)
        {
            if (i < starsAmount)
            {
                starAnimators[i].enabled = true;
            }

           else
            {
                starAnimators[i].enabled = false;
            }
        }*/

        //buttons.SetActive(true);
    }
    
    // Mostra maior pontuação da fase
    public void ShowHighScore()
    {
        gameObject.SetActive(true);
        GetNumbers(true);

        //LvLPanel tmpPanel = FindObjectOfType<GameMenu>().tmpLvlPanel;
        labelTxt.text = "HIGH SCORE";
        //starsAmount = tmpPanel.enabledStars;
        for (int i = 0; i < scoreItens.Length; i++)
        {
            scoreItens[i].obj.SetActive(true);
            if (scoreItens[i].scoreType == ScoreType.Time)
            {
                scoreItens[i].valueTxt.text = TimeFormat(scoreItens[i].amountValue);
            }

            else
            {
                scoreItens[i].valueTxt.text = scoreItens[i].amountValue.ToString();
            }
        }
        finalObsTxt.gameObject.SetActive(false);
        for (int i = 0; i < 3; i++)
        {
            starAnimators[i].enabled = true;
            print("FOR");
            if (i < starsAmount)
            {
                starAnimators[i].SetBool("Off", false);
            }
            else
            {
                starAnimators[i].SetBool("Off", true);
                starAnimators[i].Play("StarOff");
            }
        }

        buttons.SetActive(true);
    }


    public void BeginContage(int lvlStageIndex)
    {
        gameObject.SetActive(true);
        itemIndex = 0;
        starsAmount = stPersist.stars;
        //starsAmount = FindObjectOfType<GameMenu>().lvlPanels[starsAmount].enabledStars;
        labelTxt.text = "RESULTS";
        if (stPersist.newHighScore)
        {
            finalObsTxt.text = "NEW HIGH SCORE!!!";
        }
        else
        {
            finalObsTxt.text = "(Highest score will be mantained)";
        }

        for (int i = 0; i < 3; i++)
        {
            starAnimators[i].Play("StarOff");
            starAnimators[i].SetBool("Off", true);
        }

        enabled = true;
        CountItem(itemIndex);
        //scoreItens[1].obj.SetActive(true);
    }

    // AVANÇAR AO CLICAR
    public void ToNextItem()
    {
        StopAllCoroutines();
        if (typingItems)
        {
            menuSFX.PlayClip("Points2");
            if (scoreItens[itemIndex].scoreType == ScoreType.Time)
            {
                scoreItens[itemIndex].valueTxt.text = TimeFormat(scoreItens[itemIndex].amountValue);
            }

            else
            {
                scoreItens[itemIndex].valueTxt.text = scoreItens[itemIndex].amountValue.ToString();
            }

            if (itemIndex + 1 < scoreItens.Length)
            {
                CountItem(itemIndex + 1);
            }

            else
            {
                print("PARA AS ESTRELAS");
                typingItems = false;
                typingStars = true;
                StartCoroutine(ShowStars());
            }
        }

        /*else if (typingFinalScore)
        {
            if (stPersist.newHighScore)
            {
                menuSFX.PlayClip("Win");
            }

            else
            {
                menuSFX.PlayClip("Points2");
            }
            

            finalScoreTxt.gameObject.SetActive(true);

            finalScoreTxt.text = finalScorePoints.ToString() + "p";

            finalObsTxt.gameObject.SetActive(true);

            typingFinalScore = false;

            StartCoroutine(ShowStars());
        }*/

        if (typingStars)
        {
            StopAllCoroutines();
            buttons.SetActive(true);
            finalObsTxt.gameObject.SetActive(true);
            menuSFX.PlayClip("Star");
            for (int i = 0; i < starsAmount; i++)
            {
                //if (i < starsAmount)
                //{
                starAnimators[i].enabled = true;
                starAnimators[i].SetBool("Off", false);
                starAnimators[i].SetTrigger("Score");
                //}
                typingStars = false;
            }            
        }
    }

    public void GetPreviousStageName()
    {
        FindObjectOfType<GameMenu>().lvlPanels[stPersist.stageIndex - 1].TransferLvlName();
    }

    public void CountItem(int index)
    {
        itemIndex = index;
        bool timeItem = false;
        scoreItens[index].obj.SetActive(true);

        switch (scoreItens[index].scoreType)
        {
            case ScoreType.Time:
                {
                    scoreItens[index].amountValue = stPersist.completionTime;
                    scoreItens[index].pointValue = stPersist.timeScore;
                    timeItem = true;
                }
                break;

            case ScoreType.Rubies:
                {
                    scoreItens[index].amountValue = stPersist.rubyAmount;
                    scoreItens[index].pointValue = stPersist.rubyScore;
                }
                break;

            case ScoreType.Diamonds:
                {
                    scoreItens[index].amountValue = stPersist.diamondAmount;
                    scoreItens[index].pointValue = stPersist.diamondScore;
                }
                break;
        }
        StartCoroutine(typeValue(0, scoreItens[index].amountValue, scoreItens[index].pointValue, scoreItens[index].valueTxt, timeItem));
    }

    public IEnumerator typeValue(int startValue, int finalValue, int pointValue, Text textToUpdate, bool timeFormat = false)
    {
        typingItems = true;
        //Contando caso ainda não tenha acabado
        if (startValue < finalValue)
        {
            yield return new WaitForSeconds(typeTime);
            startValue += 1;
            menuSFX.PlayWhenPossible("Points1");
            if (timeFormat)
            {
                textToUpdate.text = TimeFormat(startValue);
            }

            else
            {
                textToUpdate.text = startValue.ToString();
            }
            StartCoroutine(typeValue(startValue, finalValue, pointValue, textToUpdate, timeFormat));
        }

        //Indo pro próximo item
        else
        {
            // preenchendo texto
            //pointText.gameObject.SetActive(true);


            //menuSFX.PlayClip("Points2");


            //if (retry)
            //pointText.text = "-" + pointValue.ToString() + "p";

            //else
            //{
            //    pointText.text = pointValue.ToString() + "p";
            //}
            

            // Continuando
            if (itemIndex + 1 < scoreItens.Length)
            {
                yield return new WaitForSeconds(0.5f);
                CountItem(itemIndex + 1);
            }

            else
            {
                typingItems = false;
                StartCoroutine(ShowStars());
            }
        }

    }

    /*public IEnumerator ToFinalScore()
    {
        typingFinalScore = true;
        yield return new WaitForSeconds(1);

        finalObsTxt.gameObject.SetActive(true);

        if (stPersist.newHighScore)
        {
            menuSFX.PlayClip("Win");
        }

        else
        {
            menuSFX.PlayClip("Points2");
        }

        typingFinalScore = false;

        StartCoroutine(ShowStars());
    }*/

    public IEnumerator ShowStars()
    {
        typingStars = true;
        yield return new WaitForSeconds(1);

        for (int i = 0; i < starsAmount; i++)
        {
            //if (i < starsAmount)
            //{
            yield return new WaitForSeconds(0.5f);
            starAnimators[i].SetBool("Off", false);
            starAnimators[i].enabled = true;
            starAnimators[i].SetTrigger("Score");
            menuSFX.PlayClip("Star");
            //}
        }        
        yield return new WaitForSeconds(0.5f);
        typingStars = false;
        buttons.SetActive(true);
        finalObsTxt.gameObject.SetActive(true);
    }

    public string TimeFormat(int value)
    {
        int minutes = value / 60;
        int seconds = value - minutes * 60;
        string timeString = string.Format("{0:00}:{1:00}", minutes, seconds);

        return timeString;
    }
}
