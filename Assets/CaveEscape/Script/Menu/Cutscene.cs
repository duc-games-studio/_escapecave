﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CutsceneScript : MonoBehaviour
{

    public string sceneToLoad;

    public AsyncOperation loadOperation;

    bool cutsceneIsOver;

    private void Start()
    {
        loadOperation = SceneManager.LoadSceneAsync(sceneToLoad);
        loadOperation.allowSceneActivation = false;
    }

    private void Update()
    {
        if (loadOperation.isDone)
        {
            print("Done!");
        }

        if (cutsceneIsOver)
        {
            loadOperation.allowSceneActivation = true;
        }


    }

    public void CutsceneIsOver()
    {
        cutsceneIsOver = true;
    }
    
    public void EnableSceneActivation()
    {
        loadOperation.allowSceneActivation = true;
    }
}
