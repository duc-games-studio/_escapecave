﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class StagePersistent : MonoBehaviour
{
    [Header("Script que faz cálculos de pontuação e guarda quantas fases foram completas")]
    public static StagePersistent instance;

    private void Awake()
    {
        if (!instance)
        {
            instance = this;
            DontDestroyOnLoad(transform.gameObject);

            if (firstStart)
            {
                //ClearAllData();
                firstStart = false;
            }
            LoadData();

            if (saveData == null)
            { 
                SaveData();
            }
        }

        else
        {
            Destroy(gameObject);
        }

    }

    [Header("Dados salvos")]
    public SaveData saveData;

    [Header("Diamantes e Rubis salvos")]
    public int savedRubies, savedDiamonds;

    [Header("Número de fases completas")]
    public int completedStages = 0;

    [System.Serializable]
    public class StageData
    {
        public int enabledStars;
        [Header("Pontuação salva")]
        public int finalScore;
        public int timeValue;
        public int rubyAmount;
        public int diamondAmount;
    }

    public StageData[] stData = new StageData[3];

    [Header("Variáveis para pontuação de fases")]
    public int stageIndex;
    public bool completed, died, newHighScore;

    public int rubyAmount;

    public int diamondAmount;

    public int completionTime;

    [Header("Valores de multiplicação para cálculo de pontuação")]
    public int rubyMultiplier, diamondMultiplier, timeMultiplier;

    int stageTime, timeProduct;

    [Header("Pontuação gerada")]
    public int rubyScore, diamondScore, timeScore;

    public int finalScore;

    [Header("Estrelas obtidas com pontuação atual")]

    public int stars;

    //[HideInInspector]
    public bool newBestTime;

    public bool unlockedNext;

    // Gameplay

        //[HideInInspector]
    public bool showBeginScreen, showIntroduction;

    public bool firstStart = true;

    public void SetStageIndex(int i)
    {
        stageIndex = i;
    }

    public void SetStageTime(int time)
    {
        stageTime = time;
    }

    /*public void SetCompletedStages(int i)
    {
        completedStages = i;
    }*/


    
    public void SetCompletion(bool value)
    {
        completed = value;
    }

    /*public void CheckCompletion()
    {
        if (completed)
        {
            // FAÇA
        }
    }*/

    public void GetStageValues(int rubies, int diamonds, int time)
    {
        rubyAmount = 0;
        diamondAmount = 0;
        completionTime = 0;
        rubyAmount = rubies;
        diamondAmount = diamonds;
        completionTime = time;

        GetSavedJewels(rubies, diamonds);
    }

    public void GetSavedJewels(int ruby, int diamond)
    {
        savedRubies += ruby;
        savedDiamonds += diamond;
    }

    public void WriteSaveValues()
    {
        saveData.rubyAmount = savedRubies;
        saveData.diamondAmount = savedDiamonds;
        //saveData.completedStages = completedStages;
    }

    public int CalculateStars(int points)
    {
        return FindObjectOfType<GameMenu>().lvlPanels[stageIndex - 1].GetStars(points);
    }

    public void CalculateScore()
    {
        rubyScore = rubyMultiplier * rubyAmount;
        diamondScore = diamondMultiplier * diamondAmount;

        if (completionTime >= stageTime)
        {
            print("Maior ou igual ao tempo");
            timeProduct = 1;
        }

        else
        {
            timeProduct = stageTime - completionTime;
        }

        timeScore = timeProduct * timeMultiplier;
        finalScore = rubyScore + diamondScore + timeScore;

        if (finalScore <= 0)
        {
            finalScore = 0;
        }

        stars = CalculateStars(finalScore);
        //LvLPanel tmpLvlPanel = FindObjectOfType<GameMenu>().tmpLvlPanel;
        //stars = tmpLvlPanel.GetStars(finalScore, tmpLvlPanel.starScore);

        print(stars);

        // Salvando maior pontuação
        if (finalScore > saveData.stageData[stageIndex-1].finalScore) //PlayerPrefs.GetInt("Stage" + stageIndex + "Score"))
        {
            // Usando vários Player Prefs pois não sei se JSON funciona em WEBGL
            newHighScore = true;
            stData[stageIndex-1].diamondAmount = diamondAmount;
            stData[stageIndex-1].rubyAmount = rubyAmount;
            stData[stageIndex-1].enabledStars = stars;
            stData[stageIndex-1].timeValue = completionTime;
            stData[stageIndex-1].finalScore = finalScore;

            saveData.stageData[stageIndex - 1] = stData[stageIndex - 1];
        }

        else
        {
            newHighScore = false;
        }


        //SaveData();

        print("Ruby Score " + rubyScore);
        print("Diamond Score " + diamondScore);
        print("Time Score " + timeScore);
        print("Final Score " + finalScore);
    }


    public void AddDeath()
    {
        died = true;
    }

    public void ResetDeaths()
    {
        died = false;
    }


    /*public void SaveHighScore()
    {
        PlayerPrefs.SetInt("Stage" + stageIndex + "Score", finalScore);

        // Tempo

        PlayerPrefs.SetInt("Stage" + stageIndex + "TimeValue", completionTime);

        PlayerPrefs.SetInt("Stage" + stageIndex + "TimeScore", timeScore);


        // Rubis

        PlayerPrefs.SetInt("Stage" + stageIndex + "RubyAmount", rubyAmount);

        PlayerPrefs.SetInt("Stage" + stageIndex + "RubyScore", rubyScore);


        // Diamantes

        PlayerPrefs.SetInt("Stage" + stageIndex + "DiamondAmount", diamondAmount);

        PlayerPrefs.SetInt("Stage" + stageIndex + "DiamondScore", diamondScore);


        // Mortes

        //PlayerPrefs.SetInt("Stage" + stageIndex + "RetryAmount", deaths);

        //PlayerPrefs.SetInt("Stage" + stageIndex + "RetryScore", deathScore);
    }*/


    public void SetCompletedStages(int value)
    {
        completedStages = value;
        //SaveData();
        //PlayerPrefs.SetInt("CompletedStages", completedStages);
    }

    [ContextMenu("SAVE")]
    public void SaveData()
    {
        print("Salvar");
        SaveSystem.Save(saveData);
        Debug.Log(saveData);
    }

    [ContextMenu("LOAD")]
    public void LoadData()
    {
        print("Carregar");
        if (SaveSystem.Load() != null)
        {
            saveData = SaveSystem.Load();
            Debug.Log(saveData);
        }
        
        else
        {
            print("Sem save");
            //SaveData();
            return;
        }

        if (saveData != null)
        {
            print("Com save");
            //completedStages = saveData.completedStages;
            savedDiamonds = saveData.diamondAmount;
            savedRubies = saveData.rubyAmount;

            for (int i = 0; i < saveData.stageData.Length; i++)
            {
                stData[i] = saveData.stageData[i];
            }
        }

        /*else
        {
            print("Sem save");
            completedStages = 0;
            savedDiamonds = 0;
            savedRubies = 0;

            stData = new StageData[3];
            //for (int i = 0; i < stData.Length; i++)
            //{
            //    stData[i] = null;
            //}
        }*/
    }

    /*[ContextMenu("ClearData")]
    public void ClearAllData()
    {
        PlayerPrefs.DeleteAll();

        //PlayerPrefs.DeleteKey("Stage1Score");
        //PlayerPrefs.DeleteKey("Stage2Score");
        //PlayerPrefs.DeleteKey("Stage3Score");
        //PlayerPrefs.DeleteKey("CompletedStages");
    }*/


    public void CheckNextLock()
    {
        if (stageIndex < FindObjectOfType<GameMenu>().lvlPanels.Length)
        {
            if (stars > 0)
            {
                if (!FindObjectOfType<GameMenu>().lvlPanels[stageIndex].doNotUnlock)
                {
                    unlockedNext = true;
                }
                
                // Vê se a próxima fase está travada
                //if (FindObjectOfType<GameMenu>().lvlPanels[stageIndex].locked)
                //{
                //    FindObjectOfType<GameMenu>().lvlPanels[stageIndex].AnimateUnlock();
                //}
            }

        }

        else
        {
            unlockedNext = false;
        }
    }

    /*public void AnimateNextLock()
    {
        if (unlockedNext)
        {
            FindObjectOfType<GameMenu>().lvlPanels[stageIndex].AnimateUnlock();
        }

        unlockedNext = false;
    }*/

    public void ShowBeginScreen(bool value)
    {
        showBeginScreen = value;
    }

    public void ShowIntroduction(bool value)
    {
        showIntroduction = value;
    }

    public void CheckIntroductionBool()
    {
        if (FindObjectOfType<GameMenu>().lvlPanels[stageIndex - 1].showIntroduction)
        {
            ShowIntroduction(true);
        }

        else
        {
            ShowIntroduction(false);
        }
    }
}
