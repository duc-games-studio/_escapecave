﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.InputSystem;

public class BeginScreen : MonoBehaviour
{
    public Animator screenAnimator;

    public int screenIndex;

    public GameObject[] screens;


    public float secondsToBegin;

    float waitingTime;

    public GameObject pressKeyText;


    public Animator countNumberAnim;

    public Text countNumberTXT;

    float timeToTrigger;


    bool canStart;





    public UnityEvent CanPress;

    public UnityEvent CannotPress;


    public GameObject[] objectsToDisable;

    public SFX hudSFX;

    private void Start()
    {
        StagePersistent tmpPersist = FindObjectOfType<StagePersistent>();

        

        if (tmpPersist.showBeginScreen)
        {
            FindObjectOfType<PlayerMovement>().enabled = false;
            FindObjectOfType<CameraShake>().enabled = false;

            Time.timeScale = 0;

            AudioListener.pause = true;

            EnableObjects(false);

            waitingTime = secondsToBegin;
            timeToTrigger = secondsToBegin - 1;

            if (!tmpPersist.showIntroduction)
            {
                screenAnimator.SetTrigger("Straight2");
                screenIndex = 2;
            }

            else
            {
                ToScreen(2);
               
            }

            tmpPersist.ShowBeginScreen(false);
            tmpPersist.ShowIntroduction(false);

       
        }

        else
        {
            EnableObjects(true);
            gameObject.SetActive(false);
        }


        enabled = false;
        
    }


    public void Update()
    {
        if (screenIndex == 3)
        {
            if (waitingTime > 0)
            {
                waitingTime -= Time.unscaledDeltaTime;

                waitingTime = Mathf.Clamp(waitingTime, 0, secondsToBegin);

                countNumberTXT.text = waitingTime.ToString(format:"0.00");                

                if (waitingTime <= timeToTrigger)
                {
                    countNumberAnim.SetTrigger("Count");
                    hudSFX.PlayWhole("Proceed");
                    if (timeToTrigger - 1 >= 0)
                    {
                        timeToTrigger -= 1;
                    }

                }
            }

            else
            {
                countNumberTXT.text = "0,00";

                pressKeyText.SetActive(true);
                canStart = true;
            }
        }
        
        if (canStart)
        {
            //if (Input.touchCount > 0)
            //{
            //    StartPlay();
                //QualitySettings.SetQualityLevel(0);
            //}

            if (Application.platform == RuntimePlatform.Android)
            {
                if (Touchscreen.current.primaryTouch.isInProgress)
                // if (Input.touchCount > 0)
                {
                    StartPlay();
                }
            }

            else
            {
                if (Keyboard.current.anyKey.wasPressedThisFrame || Mouse.current.press.wasPressedThisFrame)
                {
                    StartPlay();
                }
            }

        }
    }

    public void EnableUpdate()
    {
        hudSFX.PlayWhole("Proceed");
        enabled = true;
    }

    public void StartPlay()
    {
        Time.timeScale = 1;
        AudioListener.pause = false;

        screenAnimator.SetTrigger("FadeOut");
        hudSFX.PlayWhole("Proceed");

        EnableObjects(true);

        FindObjectOfType<PlayerMovement>().enabled = true;
        FindObjectOfType<CameraShake>().enabled = true;

        enabled = false;
    }

    public void ToScreen(int i)
    {
        screenIndex = i;
        screenAnimator.SetTrigger("Screen" + i);

    }


    public void UpdateScreens()
    {
        for (int i = 0; i < 3 ; i++)
        {
            if (i + 1 != screenIndex)
            {
                screens[i].SetActive(false);
            }

            else
            {
                screens[i].SetActive(true);
            }
        }
    }
    

    public void CanPressButtons()
    {
        CanPress.Invoke();
    }


    public void CannotPressButtons()
    {
        CannotPress.Invoke();
    }

    public void EnableObjects(bool value)
    {
        for (int i = 0; i < objectsToDisable.Length; i++)
        {
            objectsToDisable[i].SetActive(value);
        }
    }

    public void DisableThis()
    {
        gameObject.SetActive(false);
    }
}
