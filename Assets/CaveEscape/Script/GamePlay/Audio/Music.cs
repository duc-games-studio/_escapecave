﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Music : MonoBehaviour
{
    static Music instance;

    AudioSource aSource;

    bool destroyOnEnd;

    public bool DestroyImpostor;

    private void Awake()
    {


        if (!instance)
        {
            instance = this;
            DontDestroyOnLoad(transform.gameObject);

            aSource = GetComponent<AudioSource>();
        }

        else
        {
            if (DestroyImpostor)
            {
                Destroy(instance.gameObject);
                instance = this;

                aSource = GetComponent<AudioSource>();
            }
            else
            {
                Destroy(gameObject);
            }
            
        }

        enabled = false;

    }


    public float timeToFade;

    bool fadeOut;


    [ContextMenu("FadeOut")]
    public void FadeOut(bool destroy)
    {
        enabled = true;
        fadeOut = true;

        destroyOnEnd = destroy;
    }

    private void Update()
    {
        if (fadeOut)
        {
            aSource.volume -= Time.deltaTime / timeToFade;

            if (aSource.volume <= 0)
            {
                fadeOut = false;

                if (destroyOnEnd)
                {
                    DestroyThis();
                }

                enabled = false;
            }
        }

    }

    public void DestroyThis()
    {
        Destroy(gameObject);
    }
}
