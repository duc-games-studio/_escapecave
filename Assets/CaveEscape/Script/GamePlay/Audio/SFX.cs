﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SFX : MonoBehaviour
{
    [System.Serializable]
    public class Clip
    {
        public AudioClip audioClip;
        public string audioName;
        [Range(0,1)]
        public float volume = 1;
    }

    public AudioSource audioSource;
    public Clip[] clips;

    public void PlayClip(string audioName)
    {
        for (int i = 0; i < clips.Length; i++)
        {
            if (clips[i].audioName == audioName)
            {
                audioSource.clip = clips[i].audioClip;
                audioSource.volume = clips[i].volume;

                /*if (doNotInterrupt)
                {
                    if (!audioSource.isPlaying)
                    {
                        audioSource.Play();
                    }
                }*/

                //else
                    audioSource.Play();
                //{
                //}
                
            }
        }
    }

    public void PlayWhole(string audioName)
    {
        audioSource.ignoreListenerPause = true;
        PlayClip(audioName);//, doNotInterrupt);
    }

    public void PlayWhenPossible(string audioName)
    {
        if (!audioSource.isPlaying)
        {
            PlayClip(audioName);
        }
    }

}
