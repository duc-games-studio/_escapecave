﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StageInfo : MonoBehaviour
{
    public string sceneName;

    public string nextScene;

    public int stageNumber;

    string sceneToLoad;

    bool completed;

    public void Completed(bool value)
    {
        completed = value;

        if (value)
        {
            sceneToLoad = nextScene;
            FindObjectOfType<StagePersistent>().SetCompletion(completed);
        }

        else
        {
            sceneToLoad = sceneName;
        }
    }

    public void ToMenu()
    {
        sceneToLoad = "Menu";
    }

    public void LoadScene()
    {
        SceneManager.LoadScene(sceneToLoad);
    }
}
