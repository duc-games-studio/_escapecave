﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;
using UnityEditor;

// SCRIPT QUE FAZ O SCANEAMENTO DE OBSTÁCULOS NA FASE E GUARDA A "MATRIZ 3D"
public class LevelCoordinates : MonoBehaviour
{
    /////////// MÁSCARA \\\\\\\\\\\
    


    [Header("Camadas onde estão os obstáculos")]
    // Camadas onde estão os objetos de obstáculos, para serem detectados no Scan
    public LayerMask detectLayers;









    /////////// CLASSES PARA ESTRUTURAR A "MATRIZ 3D" \\\\\\\\\\\\\

    // Classe para se definir as colunas da "Matriz 3D"
    // Cada objeto "Lanes" corresponde a uma unidade no Eixo X
    [System.Serializable]
    public class Lanes
    {
        public List<Lines> ZAxis = new List<Lines>(100);
    }

    // Classe para se definir as linhas da "Matriz 3D"
    // Cada objeto "Lines" corresponde a uma unidade no Eixo Z
    [System.Serializable]
    public class Lines
    {
        public List<Unit> units = new List<Unit>(3);
    }

    // Classe de unidade, define o tipo de objeto e sua altura
    // Cada unidade representa um obstáculo, sendo "yHeight" a altura em que se encontra no eixo Y
    [System.Serializable]
    public class Unit
    {
        public UnitType unitType;


        public GameObject unitObject;
        public ObstacleType obstacleType;

        public CollectableType collectableType;

        public TrailType trailType;
        public TrailType inverseTrailType;
        public int yHeight;
    }

    // Cria objeto da classe "Lanes", que corresponde a cada coluna da "Matriz 3D"
    public Lanes[] xAxis = new Lanes[5];

    // Largura do cenário
    public int lvlWidth;

    [HideInInspector]
    public CollectableType tmpColType;












    [ContextMenu("ScanLevel")]
    public void ScanLevel()
    {
        CleanMatrix();

        Scene scene = SceneManager.GetActiveScene();

        GameObject[] allObjects = FindObjectsOfType<GameObject>();

        foreach(GameObject ob in allObjects)
        {
            if (ob.activeInHierarchy)
            {
                ScanObject(ob);
            }
        }

        //foreach(GameObject obj in tmpObstacles)
        //{
        //   ScanObject(obj);
        //}
    }

    public void ScanObject(GameObject obj)
    {

 

        if ((detectLayers.value & (1 << obj.gameObject.layer)) > 0)
        {

            UnitType type = new UnitType();

            if (obj.layer == LayerMask.NameToLayer("Obstacle"))
            {
                type = UnitType.Obstacle;
            }

            else if (obj.layer == LayerMask.NameToLayer("Collectable"))
            {
                type = UnitType.Collectable;                
            }

            else if (obj.layer == LayerMask.NameToLayer("Trail"))
            {
                type = UnitType.Trail;
            }

            Debug.Log(type);
            Debug.Log(obj.name);

            AddToMatrix(obj, type);
        }

    }

    // Método que pega objeto de obstáculo e suas informações para adicioná-lo na "Matriz 3D"
    public GameObject AddToMatrix(GameObject obj, UnitType unt)
    {
        // Criando unidade temporária
        Unit tmpUnit = new Unit();
        // Atualizando unidade temporária com atributos do obstáculo a ser adicionado
        tmpUnit.unitObject = obj;

        int x = (int)obj.transform.position.x;
        int y = (int)obj.transform.position.y;
        int z = (int)obj.transform.position.z;



        tmpUnit.unitType = unt;

        print(obj.name);

        if (unt == UnitType.Obstacle)
        {
            tmpUnit.obstacleType = tmpUnit.unitObject.GetComponent<Obstacle>().obstacleType;
        }

        else if (unt == UnitType.Collectable)
        {
            tmpUnit.collectableType = tmpUnit.unitObject.GetComponent<Collectable>().collectableType;
        }

        else if (unt == UnitType.Trail)
        {
            tmpUnit.trailType = tmpUnit.unitObject.GetComponent<Trail>().trailType;
            tmpUnit.inverseTrailType = tmpUnit.unitObject.GetComponent<Trail>().inverseTrailType;
        }
        
        tmpUnit.yHeight = (int)tmpUnit.unitObject.transform.position.y;

        // Print de informações do objeto, pra checar se está correto)
        Debug.Log(tmpUnit.unitObject.name + " " + x + ", " + y + ", " + z);

        // Adicionado objeto à "Matriz 3D"
        xAxis[x].ZAxis[z].units.Add(tmpUnit);

        for (int i = 0; i < xAxis[x].ZAxis[z].units.Count; i++)
        {
            if(xAxis[x].ZAxis[z].units[i] == tmpUnit)
            {
                xAxis[x].ZAxis[z].units[i].unitObject = obj;
                print("MESMO");
            }
        }

        return tmpUnit.unitObject;

    }

    // Método que limpa a "Matriz 3D"
    [ContextMenu("CleanMatrix")]
    public void CleanMatrix()
    {
        foreach(Lanes lane in xAxis)
        {
            for (int i = 0; i < lane.ZAxis.Count; i++)
            {
                lane.ZAxis[i].units.Clear();
            }
        }
    }

    // Pega os objetos em determinada posição e adiciona para uma lista de unidades
    public void FillUnitList(List<Unit> unitList, int x, int y, int z)
    {
        unitList.Clear();

        if (xAxis[x].ZAxis[z].units.Count > 0)
        {
            foreach (Unit un in xAxis[x].ZAxis[z].units)
            {
                unitList.Add(un);
            }
        }

    }

    public bool FindObstacle(ObstacleType type, int x, int y, int z, bool checkHeight)
    {
        for (int i = 0; i < xAxis[x].ZAxis[z].units.Count; i++)
        {
            if (xAxis[x].ZAxis[z].units[i].unitType == UnitType.Obstacle)
            {
                    if (xAxis[x].ZAxis[z].units[i].obstacleType == type)
                    {

                        if (checkHeight)
                        {
                            if (xAxis[x].ZAxis[z].units[i].yHeight == y)
                            {
                                return true;
                            }

                        }

                        else
                        {
                            return true;
                        }
                    }
            }
        }

        return false;
    }

    public bool FindObstacleList(ObstacleType[] type, int x, int y, int z, bool checkHeight)
    {
        print("FINDLIST: X=" + x +" Y=" + y +" Z=" + z);
        for (int i = 0; i < xAxis[x].ZAxis[z].units.Count; i++)
        {
            if (xAxis[x].ZAxis[z].units[i].unitType == UnitType.Obstacle)
            {
                for (int j = 0; j < type.Length; j++)
                {
                    if (xAxis[x].ZAxis[z].units[i].obstacleType == type[j])
                    {

                        if (checkHeight)
                        {
                            if (xAxis[x].ZAxis[z].units[i].yHeight == y)
                            {
                                return true;
                            }

                        }

                        else
                        {
                            return true;
                        }
                    }
                }

            }            
        }

        return false;
    }

    public bool FindCollectable(int x, int y, int z, bool collect)
    {
        for (int i = 0; i < xAxis[x].ZAxis[z].units.Count; i++)
        {
            if (xAxis[x].ZAxis[z].units[i].unitType == UnitType.Collectable)
            {
                if (xAxis[x].ZAxis[z].units[i].yHeight == y)
                {
                    if (collect)
                    {
                        tmpColType = xAxis[x].ZAxis[z].units[i].collectableType;
                        xAxis[x].ZAxis[z].units[i].unitObject.gameObject.GetComponent<Collectable>().CollectEvent();
                    }                   
                    return true;
                }
                
            }
        }

        return false;
    }

    public CollectableType ReturnCollectableType(int x, int y, int z)
    {
        for (int i = 0; i < xAxis[x].ZAxis[z].units.Count; i++)
        {
            if (xAxis[x].ZAxis[z].units[i].unitType == UnitType.Collectable)
            {
                if (xAxis[x].ZAxis[z].units[i].yHeight == y)
                {
                    return xAxis[x].ZAxis[z].units[i].collectableType;//unitObject.GetComponent<Collectable>();
                }
            }
        }
        return CollectableType.Diamond;
    }

    public GameObject ReturnObstacle(ObstacleType type, int x, int y, int z, bool checkHeight)
    {
        for (int i = 0; i < xAxis[x].ZAxis[z].units.Count; i++)
        {
            if (xAxis[x].ZAxis[z].units[i].obstacleType == type)
            {
                if (checkHeight)
                {
                    if (xAxis[x].ZAxis[z].units[i].yHeight == y)
                    {
                        return xAxis[x].ZAxis[z].units[i].unitObject;
                    }
                }

                else
                {
                    return xAxis[x].ZAxis[z].units[i].unitObject;
                }
            }
        }

        return null;
    }

    public Obstacle ReturnAnyObstacle(int x, int y, int z)
    {
        for (int i = 0; i < xAxis[x].ZAxis[z].units.Count; i++)
        {
            if (xAxis[x].ZAxis[z].units[i].unitType == UnitType.Obstacle)
            {
                if (xAxis[x].ZAxis[z].units[i].yHeight == y)
                {
                    return xAxis[x].ZAxis[z].units[i].unitObject.GetComponent<Obstacle>();
                }
            }
        }

        return null;
    }
    public bool FindTrail(TrailType trailType, int x, int y, int z, bool inverse)
    {
        for (int i = 0; i < xAxis[x].ZAxis[z].units.Count; i++)
        {
            if (xAxis[x].ZAxis[z].units[i].unitType == UnitType.Trail)
            {
                if (!inverse)
                {
                    if (xAxis[x].ZAxis[z].units[i].trailType == trailType)
                    {
                        if (xAxis[x].ZAxis[z].units[i].yHeight == y)
                        {
                            return true;
                        }
                    }
                }

                else
                {
                    if (xAxis[x].ZAxis[z].units[i].inverseTrailType == trailType)
                    {
                        if (xAxis[x].ZAxis[z].units[i].yHeight == y)
                        {
                            return true;
                        }
                    }
                }

            }
        }

        return false;
    }

    public bool FindTag(string tag, int x, int y, int z, bool checkHeight)
    {
        for (int i = 0; i < xAxis[x].ZAxis[z].units.Count; i++)
        {
            if (xAxis[x].ZAxis[z].units[i].unitObject.CompareTag(tag))
            {
                if (checkHeight)
                {
                    if (xAxis[x].ZAxis[z].units[i].yHeight == y)
                    {                    
                        return true;
                    }

                    else
                    {
                        return false;
                    }
                }

                else
                {
                    return true;
                }
                

            }
        }

        return false;
    }

    public void RemoveUnit(GameObject obj, int x, int z)
    {
        //int x = (int)obj.transform.position.x;
        //int z = (int)obj.transform.position.z;

        for (int i = 0; i < xAxis[x].ZAxis[z].units.Count; i++)
        {
            if (xAxis[x].ZAxis[z].units[i].unitObject == obj)
            {
                xAxis[x].ZAxis[z].units.Remove(xAxis[x].ZAxis[z].units[i]);
            }
        }

    }

    public void RemoveUnitOnSpot(GameObject obj, int x, int z)
    {
        for (int i = 0; i < xAxis[x].ZAxis[z].units.Count; i++)
        {
            if (xAxis[x].ZAxis[z].units[i].unitObject == obj)
            {
                xAxis[x].ZAxis[z].units.Remove(xAxis[x].ZAxis[z].units[i]);
            }
        }
    }

    //[CustomEditor(typeof(LevelCoordinates))]
    //public class MyEditorClass : Editor
    //{
        //public override void OnInspectorGUI()
        //{
        //    base.OnInspectorGUI();

        //    Debug.Log("Areia");
        //}
    //}

}
