﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Fader : MonoBehaviour
{
    Animator anim;

    public UnityEvent fadeOutEvent;

    public UnityEvent quitEvent;

    private void Start()
    {
        anim = GetComponent<Animator>();        
    }

    public void FadeIn()
    {
        anim.SetBool("FadeIn", true);
    }

    public void FadeOut()
    {
        anim.SetBool("FadeIn", false);
    }

    public void InvokeFadeOutEvent()
    {
        fadeOutEvent.Invoke();
    }

    public void InvokeQuitEvent()
    {
        quitEvent.Invoke();
    }
}
