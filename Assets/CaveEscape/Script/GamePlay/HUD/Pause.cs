﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Pause : MonoBehaviour
{
    public UnityEvent pauseEvent;
    public UnityEvent unpauseEvent;

    bool canPause = true;

    //public GameObject pauseButton;

    //public PlayerMovement playerScript;

    public void PauseGame(bool value)
    {        
        if (canPause)
        {
            if (value)
            {
                print("Pause");
                pauseEvent.Invoke();

                
            }

            else
            {
                unpauseEvent.Invoke();

            }

            AudioListener.pause = value;
        }
    }

    public void TimeZero(bool value)
    {
        if (value)
        {
            Time.timeScale = 0;
        }

        else
        {
            Time.timeScale = 1;
        }
    }

    public void CanPause(bool value)
    {
        canPause = value;
    }
}
