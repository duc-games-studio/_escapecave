﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

// SCRIPT QUE ADMINISTRA VISUAIS DA HUD (Painel de joias, item, etc)
public class HUDManager : MonoBehaviour
{
    [Header("Painel de jóias")]
    public Text rubyTXT;
    public Animator rubyTxtAnimator;
    public GameObject rubyObject;

    public Text diamondTXT;
    public Animator diamondTxtAnimator;
    public GameObject diamondObject;

    [Header("Pause")]
    public Animator pauseAnimator;

    [Header("Painel de Itens")]
    public Animator itemPanelAnimator;
    public GameObject itemArrow;

    int rubyTxtAmount, diamondTxtAmount;

    public void UpdateRubiTXT()
    {
        rubyTxtAnimator.SetTrigger("Get");
        //rubyTXT.text = GameManager.instance.score.rubyAmount.ToString();

        rubyTxtAmount++;
        rubyTXT.text = rubyTxtAmount.ToString();
        //rubyTxtAmount = GameManager.instance.score.rubyAmount;
    }

    public void UpdateDiamondTXT()
    {
        diamondTxtAnimator.SetTrigger("Get");
        diamondTxtAmount ++;//= GameManager.instance.score.diamondAmount;
        diamondTXT.text = diamondTxtAmount.ToString(); //GameManager.instance.score.diamondAmount.ToString();

    }

    public void ShowItemArrow(bool value)
    {
        itemArrow.SetActive(value);
    }

    public void UpdateArrowOrientation(Directions direction)
    {
        switch (direction)
        {
            case Directions.Front:
                {
                    itemArrow.transform.rotation = Quaternion.Euler(0, 0, 0);
                }
                break;

            case Directions.Back:
                {
                    itemArrow.transform.rotation = Quaternion.Euler(0, 0, 180);
                }
                break;

            case Directions.Left:
                {
                    itemArrow.transform.rotation = Quaternion.Euler(0, 0, 90);
                }
                break;

            case Directions.Right:
                {
                    itemArrow.transform.rotation = Quaternion.Euler(0, 0, 270);
                }
                break;
        }

    }

    public void PauseFade(bool value)
    {
        pauseAnimator.SetBool("Pause", value);
    }

    public void AreYouSure(bool value)
    {
        if (value)
        {
            pauseAnimator.SetTrigger("Sure");
        }

        else
        {
            pauseAnimator.SetTrigger("Unsure");
        }
    }

    public void QuitFromPause()
    {
        FindObjectOfType<StagePersistent>().SetCompletion(false);

        pauseAnimator.SetTrigger("Quit");
    }


    public void ShowItemPanel(bool value)
    {
        itemPanelAnimator.SetBool("Show", value);
    }


    public IEnumerator typeDiamondTXT()
    {
        if (diamondTxtAmount < GameManager.instance.score.diamondAmount)
        {
            yield return new WaitForSeconds(0.05f);
            diamondTxtAmount++;

            diamondTxtAnimator.SetTrigger("Get");
            diamondTXT.text = diamondTxtAmount.ToString();

            StartCoroutine(typeDiamondTXT());
        }
    }
}
