﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraManager : MonoBehaviour
{
    [Header("Corpo da câmera (CameraHolder)")]
    public Transform cameraBody;

    [Header("Corpo a ser seguido")]
    public Transform bodyToFollow;

    [Header("Posição mínima e máxima no eixo X")]
    public float minX;
    public float maxX;

    [Header("Posição mínima e máxima no eixo Y")]
    public float minY;
    public float maxY;

    [Header("Posição mínima e máxima no eixo Z")]
    public float minZ;
    public float maxZ;

    [Header("Suavização")]
    public float smooth = 0.1f;

    float distance;

    private void FixedUpdate()
    {
        float xPos = Mathf.Clamp(bodyToFollow.position.x, minX, maxX);

        float yPos = Mathf.Clamp(bodyToFollow.position.y, minY, maxY);

        float zPos = Mathf.Clamp(bodyToFollow.position.z, minZ, maxZ);

        distance = Vector3.Distance(cameraBody.position, bodyToFollow.position);
        
        Vector3 myPosition = new Vector3(xPos, yPos, zPos);

        Vector3 finalPosition = Vector3.Lerp(cameraBody. position, myPosition, smooth*distance);

        cameraBody.transform.position = finalPosition;
    }
}
