﻿ using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// SCRIPT QUE GUARDA PONTUAÇÃO E MÉTODOS PARA ALTERÁ-LA
public class Score : MonoBehaviour
{
    [Header("Quantidade de Rubis")]
    public int rubyAmount;
    [Header("Quantidade de Diamantes")]
    public int diamondAmount;

    public int completionTime;

    int timeWhenStarted;

    private void Start()
    {
        timeWhenStarted = (int)Time.time;
    }

    public void AddRuby(int amount = 1)
    {
        rubyAmount += amount;
    }

    public void AddDiamond(int amount = 1)
    {
        diamondAmount += amount;
    }

    public void GetCompletionTime()
    {
        completionTime = (int)Time.time - timeWhenStarted;
    }

    public void TransferValues()
    {
        GetCompletionTime();
        FindObjectOfType<StagePersistent>().GetStageValues(rubyAmount, diamondAmount, completionTime);

        print("Rubis " + rubyAmount);
        print("Diamantes " + diamondAmount);
        print("Tempo " + completionTime);
    }
}
