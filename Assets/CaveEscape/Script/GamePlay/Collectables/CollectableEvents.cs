﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

// SCRIPT QUE GUARDA EVENTOS A SEREM REALIZADOS AO COLETAR ITENS
public class CollectableEvents : MonoBehaviour
{
    [Header("0-Rubi, 1-Diamante")]
    [Header("Estruturado dessa forma para evitar uso de 'for'")]
    public UnityEvent[] collectEvents;

    public GameObject diamondPrefab;

    public void InvokeEvent(int i)
    {
        collectEvents[i].Invoke();
    }

    public void InsertDiamonds(int amount, Vector3 position, SFX audioToPlay)
    {
        StartCoroutine(Diamonds(amount, position, audioToPlay));
    }

    IEnumerator Diamonds(int amount, Vector3 position, SFX audioToPlay)
    {
        int count = 0;

        while (count < amount)
        {
            Collectable tmpDiamond = Instantiate(diamondPrefab, position, transform.rotation).GetComponent<Collectable>();
            tmpDiamond.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);

            tmpDiamond.enabled = true;
            tmpDiamond.StartLerp();

            audioToPlay.PlayClip("Get");

            count++;
            yield return new WaitForSeconds(0.1f);
        }
    }
}
