﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// SCRIPT QUE DEFINE QUE TIPO DE COLETÁVEL É O OBJETO E EVENTO A SER EXECUTADO NA COLETA
public class Collectable : MonoBehaviour
{
    [Header("Tipo de coletável")]
    // Enum que diz qual o tipo de coletável é o objeto
    public CollectableType collectableType;

    GameObject hudObject;
    Vector3 start, destination;
    public float timeToLerp = 0.5f;
    float percentage;

    public bool hudInteraction = true;
    private void FixedUpdate()
    {
        percentage += Time.deltaTime / timeToLerp;
        destination = Camera.main.ScreenToWorldPoint(hudObject.transform.position);
        transform.position = Vector3.Lerp(start, destination, percentage);

        if (percentage >= 1)
        {
            EndLerp();
        }
    }

    public void EndLerp()
    {
        switch (collectableType)
        {
            case CollectableType.Diamond:
                {
                    GameManager.instance.hudManager.UpdateDiamondTXT();
                }
                break;

            case CollectableType.Rubi:
                {
                    GameManager.instance.hudManager.UpdateRubiTXT();
                }
                break;
        }

        gameObject.SetActive(false);
    }

    public void StartLerp()
    {
        switch (collectableType)
        {
            case CollectableType.Diamond:
                {
                    hudObject = GameManager.instance.hudManager.diamondObject;
                }
                break;

            case CollectableType.Rubi:
                {
                    hudObject = GameManager.instance.hudManager.rubyObject;
                }
                break;
        }

        start = transform.position;        

        enabled = true;
    }

    // Método que realiza evento durante a coleta do objeto
    public void CollectEvent(bool remove = true)
    {
        // Dispara evento relacionado ao tipo de coletável, depois desativa o objeto
        GameManager.instance.colEvents.InvokeEvent((int)collectableType);

        if (remove)
        {
            GameManager.instance.lvlCoordinates.RemoveUnit(gameObject, (int)transform.position.x, (int)transform.position.z);
        }
        
        if (hudInteraction)
        {
            StartLerp();
        }

        else
        {
            gameObject.SetActive(false);
        }
    }
}
