using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Controller : MonoBehaviour
{
    public UnityEvent holdingEvent;
    public Animator animator;

    public bool holding;

    public void Holding(bool value)
    {
        holding = value;
    }

    public void ButtonSelected(bool value)
    {
        animator.SetBool("Selected", value);
    }

    void Update()
    {
        if (holding)
        {
            holdingEvent.Invoke();
        }
    }
}
