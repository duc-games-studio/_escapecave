using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class InputManagement : MonoBehaviour
{
    [System.Serializable]
    public class InputKey
    {
        public KeyCode[] keys;
        public UnityEvent keyEvent;
    }

    [Header("0 = Up, 1 = Back, 2 = Left, 3 = Right, 4 = Item")]
    public InputKey[] inputKeys;

    public PlayerMovement playerScript;

    private void Start()
    {
        if (Application.platform == RuntimePlatform.Android)
        {
            enabled = false;
        }
    }

    private void Update()
    {
        if (playerScript.enabled)
        {
            CheckKeys();
        }
    }

    public void CheckKeys()
    {
        for (int i = 0; i < inputKeys.Length; i++)
        {
            for (int j = 0; j < inputKeys[i].keys.Length; j++)
            {
                if (Input.GetKey(inputKeys[i].keys[j]))
                {
                    inputKeys[i].keyEvent.Invoke();
                }
            }
        }
    }
}
