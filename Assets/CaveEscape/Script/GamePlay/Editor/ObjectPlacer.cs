﻿
using UnityEngine;
using UnityEditor;

public class ObjectPlacer : EditorWindow
{


    ///// SCRIPTABLE OBJ \\\\\
    
    // Container de objetos a serem usados para popular o mapa
    public ObjectsContainer currentContainer;


    public UnitType unitType;


    ///// ENUM \\\\\

    // Enum para escolher qual objeto do tipo obstáculo vai ser instanciado
    public ObstacleType obstacleToPlace;


    public CollectableType collectableToPlace;


    public TrailType trailToPlace;
    public Directions direction;

    ///// VECTOR \\\\\

    // Vector que guarda posição onde o objeto vai ser instanciado
    public Vector3Int spawnPosition;


    public bool manual;


    public bool fixedHeight;

    public int height;

    ///// ORIENTADOR \\\\\

    // Objeto que serve para orientação visual
    public GameObject orientator;
    public GameObject orientatorPrefab;

    // Local onde a janela é localizada e método para abri-la.
    [MenuItem("Window/ObjectPlacer")]
    public static void ShowWindow()
    {
        GetWindow<ObjectPlacer>();
    }

    
    void OnGUI()
    {

        // Linha de texto que aparece na janela do editor¹
        EditorGUILayout.LabelField("Container de objetos");

        // Permite alterar qual vai ser o container de objetos a ser usado
        currentContainer = (ObjectsContainer)EditorGUILayout.ObjectField(currentContainer, typeof(ScriptableObject), false);

        // Linha de texto que aparece na janela do editor²
        EditorGUILayout.LabelField("Tipo de objeto a ser instanciado");

        unitType = (UnitType)EditorGUILayout.EnumPopup(unitType);

        if (unitType == UnitType.Obstacle)
        {

            EditorGUILayout.LabelField("Tipo de Obstáculo");

            // Deixa visível o enum de opções de obstáculos
            obstacleToPlace = (ObstacleType)EditorGUILayout.EnumPopup(obstacleToPlace);

            if (obstacleToPlace == ObstacleType.MineCart)
            {
                EditorGUILayout.LabelField("Direção");
                direction = (Directions)EditorGUILayout.EnumPopup(direction);
            }
        }

        else if (unitType == UnitType.Collectable)
        {

            EditorGUILayout.LabelField("Tipo de Coletável");

            // Deixa visível o enum de opções de coletáveis
            collectableToPlace = (CollectableType)EditorGUILayout.EnumPopup(collectableToPlace);
        }
        
        else if (unitType == UnitType.Trail)
        {
            EditorGUILayout.LabelField("Tipo de Trilho");

            
            // Deixa visível o enum de opções de trilhos
            trailToPlace = (TrailType)EditorGUILayout.EnumPopup(trailToPlace);


            // Deixa visível a direção para ser colocado o trilho

            EditorGUILayout.LabelField("Direção anterior do trilho");

            direction = (Directions)EditorGUILayout.EnumPopup(direction);

        }




        // Deixa visível o Vector3 para definir a posição do objeto
        spawnPosition = EditorGUILayout.Vector3IntField("Posição de instanciamento", spawnPosition);


        // Bool que permite posicionamento ser feito com o mouse
        EditorGUILayout.LabelField("Posicionamento manual (Barra de espaço para instanciar)");
        manual = EditorGUILayout.Toggle(manual);

        if (manual)
        {
            EditorGUILayout.LabelField("Altura fixa");
            fixedHeight = EditorGUILayout.Toggle(fixedHeight);

            if (fixedHeight)
            {
                height = EditorGUILayout.IntField(height);
            }
        }
        

        ///////ESPAÇO
        EditorGUILayout.Space(20);        
        EditorGUILayout.LabelField("Inserir Objeto");
        // Cria botão na janela e método a ser executado for pressionado
        if (GUILayout.Button("Spawn"))
        {
            SearchAndPlace();
        }

        if (Selection.gameObjects.Length > 0)
        {
            EditorGUILayout.Space(10);    
            EditorGUILayout.LabelField("Substituir Objeto");
            if (GUILayout.Button("Replace"))
            {
                        GameObject[] selectedObjs = Selection.gameObjects;
                        for (int i = 0; i < selectedObjs.Length; i++)
                        {
                            bool correctType = false;

                            spawnPosition = new Vector3Int((int)selectedObjs[i].transform.position.x
                            ,(int)selectedObjs[i].transform.position.y
                            ,(int)selectedObjs[i].transform.position.z);

                            switch (unitType)
                            {
                                case UnitType.Obstacle:
                                {
                                    if (selectedObjs[i].GetComponent<Obstacle>())                            
                                    {
                                        correctType = true;
                                    }
                                }
                                break;

                                case UnitType.Collectable:
                                {
                                    if (selectedObjs[i].GetComponent<Collectable>())                            
                                    {
                                        correctType = true;
                                    }
                                }
                                break;

                                case UnitType.Trail:
                                {
                                    if (selectedObjs[i].GetComponent<Trail>())                            
                                    {
                                        correctType = true;
                                    }
                                }
                                break;
                            }
                            
                            if (correctType)
                            {
                                        DestroyImmediate(selectedObjs[i]);
                                        SearchAndPlace(true);
                            }
                        }

            }
        }


        ///////ESPAÇO
        EditorGUILayout.Space(20);




        // Orientação visual
        EditorGUILayout.LabelField("Objeto para orientação visual");

        if (orientator != null)
        {
            if (orientator.activeSelf && !manual)
            {
                MoveOrientatorToSpawnPos();
            }

            if (GUILayout.Button("ShowOrientator"))
            {
                orientator.SetActive(true);
            }

            if (GUILayout.Button("HideOrientator"))
            {
                orientator.SetActive(false);
            }

        }
        else




        ///////ESPAÇO
        EditorGUILayout.Space(20);




        EditorGUILayout.LabelField("Scaneia o cenário, adicionando os obstáculos à lista");
        if (GUILayout.Button("ScanLevel"))
        {
            FindObjectOfType<LevelCoordinates>().ScanLevel();
        }

        EditorGUILayout.LabelField("Esvazia a lista de obstáculos");
        if (GUILayout.Button("ClearList"))
        {
            FindObjectOfType<LevelCoordinates>().CleanMatrix();
        }
        // Botão que quando pressionado, deleta o objeto destacado na Unity
        /*if (GUILayout.Button("DeleteSelected"))
        {
            DestroyImmediate(Selection.activeGameObject);
        }*/


    }

    void OnScene(SceneView scene)
    {
        Event e = Event.current;

        if (manual)
        {
            Vector3 mousePos = e.mousePosition;
            float ppp = EditorGUIUtility.pixelsPerPoint;

            mousePos.y = scene.camera.pixelHeight - mousePos.y * ppp;

            mousePos.x *= ppp;

            Ray ray = scene.camera.ScreenPointToRay(mousePos);

            RaycastHit hit;

            if (Physics.Raycast(ray, out hit))
            {
                Vector3Int intPos = new Vector3Int((int)hit.point.x, (int)hit.point.y, (int)hit.point.z);

                intPos.x = Mathf.Clamp(intPos.x, 0, 6);

                if (fixedHeight)
                {
                    intPos.y = height;
                }

                if (intPos.z < 0)
                {
                    intPos.z = 0;
                }

                Debug.Log(intPos);
                spawnPosition = intPos;

                MoveOrientatorToSpawnPos();

                
            }

            switch (e.type)
            {
                case EventType.KeyDown:
                    {
                        if (Event.current.keyCode == KeyCode.Space)
                        {
                            SearchAndPlace();

                            e.Use();
                        }
                        break;
                    }

            }


        }

    }

    private void OnEnable()
    {

        SceneView.duringSceneGui += OnScene;
        //if (orientator != null)
        //{
        //    orientator = Instantiate(orientator, spawnPosition, orientator.transform.rotation);
        //}
        SpawnNewOrientator();
    }

    private void OnDestroy()
    {
        if (orientator != null)
        {
            DestroyImmediate(orientator);
        }

        manual = false;
    }

    // Busca o objeto desejado e o instancia
    void SearchAndPlace(bool replace = false)
    {
        if (unitType == UnitType.Obstacle)
        {
            // Localiza o objeto do tipo escolhido (objectToPlace) no container de objetos (currentContainer) e o instancia na posição desejada
            foreach (ObjectsContainer.ObstacleList obj in currentContainer.obstacleList)
            {
                if (obstacleToPlace == obj.theType)
                {

                    // Se houver mais de um objeto do mesmo tipo, um deles é sorteado para ser colocado no mapa
                    if (obj.theObstacle.Length > 1)
                    {
                        int i = Random.Range(0, obj.theObstacle.Length);


                        if (obj.theType == ObstacleType.MineCart)
                        {
                            obj.theObstacle[i].GetComponent<Minecart>().SetDirection(direction);
                            PlaceObject(obj.theObstacle[i], replace);
                        }

                        else
                        {
                            PlaceObject(obj.theObstacle[i], replace);
                        }
                    }

                    // Se houver apenas um objeto do tipo escolhido, ele é instanciado
                    else
                    {
                        

                        if (obj.theType == ObstacleType.MineCart)
                        {
                            obj.theObstacle[0].GetComponent<Minecart>().SetDirection(direction);
                            PlaceObject(obj.theObstacle[0], replace);
                        }

                        else
                        {
                            PlaceObject(obj.theObstacle[0], replace);
                        }
                    }
                }
            }
        }


        else if (unitType == UnitType.Collectable)
        {
            foreach (ObjectsContainer.CollectableList col in currentContainer.collectableList)
            {
                if (collectableToPlace == col.collectableType)
                {
                    if (col.theCollectable.Length > 1)
                    {
                        PlaceObject(col.theCollectable[Random.Range(0, col.theCollectable.Length)], replace);
                    }

                    else
                    {
                        PlaceObject(col.theCollectable[0], replace);
                    }
                }
            }
        }



        else if (unitType == UnitType.Trail)
        {
            foreach (ObjectsContainer.TrailList tra in currentContainer.trailList)
            {
                if (trailToPlace == tra.trailType)
                {
                    if (tra.trailType == TrailType.StraightTrail || tra.trailType == TrailType.EndStraightTrail || tra.trailType == TrailType.DiagonalTrail || tra.trailType == TrailType.EndDiagonalTrail)
                    {
                        Quaternion tmpRotation = new Quaternion();


                        if (direction == Directions.Front)
                        {
                            tmpRotation.eulerAngles = new Vector3(0, 0, 0);
                        }

                        else if (direction == Directions.Back)
                        {
                            tmpRotation.eulerAngles = new Vector3(0, 180, 0);
                        }

                        else if (direction == Directions.Left)
                        {
                            tmpRotation.eulerAngles = new Vector3(0, -90, 0);
                        }

                        else if (direction == Directions.Right)
                        {
                            tmpRotation.eulerAngles = new Vector3(0, 90, 0);
                        }



                        tra.theTrail.transform.rotation = tmpRotation;
                        tra.theTrail.GetComponent<Trail>().SetInverseType(TrailType.StraightTrail);

                        PlaceObject(tra.theTrail, replace);
                    }

                    else
                    {
                        tra.theTrail.transform.localScale = new Vector3(0.625f, 0.625f, 0.625f);

                        Vector3 tmpScale = tra.theTrail.transform.localScale;

                        if (tra.trailType == TrailType.ToFrontTrail)
                        {
                            if (direction == Directions.Left)
                            {
                                tra.theTrail.transform.localScale = new Vector3(tmpScale.x, tmpScale.y, tmpScale.z);

                                tra.theTrail.GetComponent<Trail>().SetInverseType(TrailType.ToRightTrail);
                            }

                            else if (direction == Directions.Right)
                            {
                                tra.theTrail.transform.localScale = new Vector3(tmpScale.x, tmpScale.y, -tmpScale.z);

                                tra.theTrail.GetComponent<Trail>().SetInverseType(TrailType.ToLeftTrail);
                            }
                        }

                        else if (tra.trailType == TrailType.ToBackTrail)
                        {
                            if (direction == Directions.Left)
                            {
                                tra.theTrail.transform.localScale = new Vector3(tmpScale.x, tmpScale.y, tmpScale.z);

                                tra.theTrail.GetComponent<Trail>().SetInverseType(TrailType.ToRightTrail);
                            }

                            else if (direction == Directions.Right)
                            {
                                tra.theTrail.transform.localScale = new Vector3(-tmpScale.x, tmpScale.y, tmpScale.z);

                                tra.theTrail.GetComponent<Trail>().SetInverseType(TrailType.ToLeftTrail);
                            }
                        }

                        else if (tra.trailType == TrailType.ToLeftTrail)
                        {
                            if (direction == Directions.Back)
                            {
                                tra.theTrail.transform.localScale = new Vector3(tmpScale.x, tmpScale.y, -tmpScale.z);

                                tra.theTrail.GetComponent<Trail>().SetInverseType(TrailType.ToFrontTrail);
                            }

                            else if (direction == Directions.Front)
                            {
                                tra.theTrail.transform.localScale = new Vector3(tmpScale.x, tmpScale.y, tmpScale.z);

                                tra.theTrail.GetComponent<Trail>().SetInverseType(TrailType.ToBackTrail);
                            }
                        }

                        else if (tra.trailType == TrailType.ToRightTrail)
                        {
                            if (direction == Directions.Front)
                            {
                                tra.theTrail.transform.localScale = new Vector3(-tmpScale.x, tmpScale.y, tmpScale.z);

                                tra.theTrail.GetComponent<Trail>().SetInverseType(TrailType.ToBackTrail);

                            }

                            else if (direction == Directions.Back)
                            {
                                tra.theTrail.transform.localScale = new Vector3(-tmpScale.x, tmpScale.y, -tmpScale.z);

                                tra.theTrail.GetComponent<Trail>().SetInverseType(TrailType.ToFrontTrail);

                            }
                        }

                        PlaceObject(tra.theTrail, replace);
                    }
                }
            }
        }
    }

    // Método que instancia objeto na posição desejada
    void PlaceObject(GameObject objectToPlace, bool replace = false)
    {
        GameObject tmpObj = Instantiate(objectToPlace, spawnPosition, objectToPlace.transform.rotation, GameObject.Find("===OBSTÁCULOS===").transform);
        if (!replace)
        Selection.activeGameObject = tmpObj;
    }

    public void SpawnNewOrientator()
    {
        GameObject newOrientator = Instantiate(orientatorPrefab, spawnPosition, orientatorPrefab.transform.rotation);
        orientator = newOrientator;
    }
    public void MoveOrientatorToSpawnPos()
    {
        if (orientator != null)
        {
            orientator.transform.position = spawnPosition;
        }
        else
        {
            SpawnNewOrientator();
            orientator.transform.position = spawnPosition;
        }
        
    }
}


