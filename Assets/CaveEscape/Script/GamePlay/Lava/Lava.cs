﻿using UnityEngine;
using UnityEngine.Rendering;

public class Lava : MonoBehaviour
{
    public float advanceRate = 0.1f;

    public float limitRate;

    public float increaseMultiplier;

    public Transform lavaTransform;
    public CameraShake camShake;
    float originalShakeAmount;
    public GameObject player;
    float dist;



    public GameObject audioOrigin;
    public float killDistance;
    bool killed;



    void Awake()
    {
        if (lavaTransform == null)
        {
            lavaTransform = GetComponent(typeof(Transform)) as Transform;
        }
        originalShakeAmount = camShake.shakeAmount;
    }

    void FixedUpdate()
    {
        if (advanceRate < limitRate)
        {
            advanceRate += 0.0001f * increaseMultiplier;
        }

        if (transform.position.z < player.transform.position.z + 15)
        {
            lavaTransform.localPosition += new Vector3(0, 0, advanceRate);
        }
        

        if (transform.position.z < player.transform.position.z)
        {
            dist = player.transform.position.z - lavaTransform.position.z;
        }

        else
        {
            
            audioOrigin.transform.position = new Vector3(audioOrigin.transform.position.x, audioOrigin.transform.position.y, player.transform.position.z);
        }
        
        camShake.shakeAmount = Mathf.Clamp(originalShakeAmount / (0.3f*dist), originalShakeAmount*0.3f, originalShakeAmount);




        if (!killed)
        {
            if (dist < killDistance)
            {
                if (player.transform.position.y <= lavaTransform.position.y)
                {
                    player.GetComponent<PlayerMovement>().LavaDeath();
                    killed = true;
                }
            }
        }

    }
}
