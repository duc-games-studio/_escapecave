﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// SCRIPT QUE GUARDA OS ENUMS PRODUZIDOS, PRA NÃO ENROLAR TUDO

// Enum que guarda tipos de objeto/obstáculo
public enum ObstacleType
{
    Stone,
    EmeraldOre,
    Mud,
    Mushroom,
    MineCart,

    BrakedStone,
    MovableStone,
    Ladders,
    Spikes,
    Stalactite,
    Lava,
    Water,
    IceBlock,    

    StageEnd,
}

public enum TrailType
{
    StraightTrail,
    //BackTrail,
    //LeftTrail,
    //RightTrail,

    ToFrontTrail,
    ToBackTrail,
    ToLeftTrail,
    ToRightTrail,

    EndStraightTrail,
    DiagonalTrail,
    EndDiagonalTrail,

    EndStraightToFront,
    EndStraightToBack,
    EndStraightToLeft,
    EndStraightToRight,

    //FrontToRightTrail,
    //FrontToLeftTrail,

    //RightToFrontTrail,
    //RightToBackTrail,

    //LeftToFrontTrail,
    //LeftToBackTrail,

    //BackToRightTrail,
    //BackToLeftTrail,

    BrokenTrail,
}


// Enum que guarda tipos de coletável
public enum CollectableType
{
    Rubi = 0,
    Diamond = 1,
    Pickaxe = 2,
}

public enum UnitType
{

    Obstacle = 0,
    Collectable = 1,
    Trail = 2,
}

public enum Directions
{
    Front = 1,
    Back = -1,
    Left = -2,
    Right = 2,
}

public enum ScoreType
{
    Time,
    Rubies,
    Diamonds,
}


public enum PlayerState
{
    Standing,
    Walking,
    Sliding,
    Riding,
    Falling,
    Interacting,
    Jumping,
    //MovingStone,
}

