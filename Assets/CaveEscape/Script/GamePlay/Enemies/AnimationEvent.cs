using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class AnimationEvent : MonoBehaviour
{
    public UnityEvent[] events;

    public void InvokeEvent(int i)
    {
        events[i].Invoke();
    }
}
