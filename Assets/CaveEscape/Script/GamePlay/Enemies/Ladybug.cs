using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ladybug : MonoBehaviour
{
    public Vector3Int initialPos, destination, playerDistance;
    public Vector3Int alternatePath;
    int x,y,z;
    bool moving, altPath, decided, canKill = true;
    public int distanceLimit = 5, followRange = 5;

    //public float minSpeed, maxSpeed;
    //float currentSpeed;

    public float moveSpeed;

    public float minWaitTime, maxWaitTime, attackTime;
    public float timeToWait, timeSinceWaiting, movePercentage;
    Directions direction;
    //Direções para seguir o jogador
    public Directions horizontalDirection, verticalDirection;
    bool zeroHorizontal, zeroVertical;
    public Transform playerTransform, lavaWallTransform;
    public Animator anim;
    public SFX sfx, insectNoise;
    public ParticleSystem lavaParticle, deathParticle;

    LevelCoordinates lvlCoord;
    private void Start() 
    {
        GetPosition();
        initialPos = new Vector3Int(x,y,z);
        RestartTime();
        playerTransform = FindObjectOfType<PlayerMovement>().transform;
        lavaWallTransform = FindObjectOfType<Lava>().transform;
        lvlCoord = FindObjectOfType<LevelCoordinates>();

        insectNoise.audioSource.loop = true;
        insectNoise.PlayClip("Noise");
    }

    private void FixedUpdate() 
    {        
        if (moving)
        {
            //movePercentage += Time.deltaTime * moveSpeed;//currentSpeed;
            transform.position = Vector3.MoveTowards(transform.position, destination, moveSpeed * Time.deltaTime);

            if (transform.position == destination)
            {
                transform.position = destination;
                anim.SetBool("Move",false);
                GetPosition();

                RestartTime();
                moving = false;

                if (altPath)
                {
                    if (transform.position == alternatePath)
                    {
                        altPath = false;
                    }
                }
            }
        }        

        else
        {
            print("eLSE");
            if (timeSinceWaiting < timeToWait)
            {
                timeSinceWaiting += Time.deltaTime;
                decided = false;
            }

            else
            {
                if (!decided)
                {
                    DecideMovement();
                    decided = true;
                }
                //SetDestination(x,y,z + 1);
                //StartMovement();
            }
        }

        CollisionCheck();
    }

    public void RestartTime()
    {
        timeSinceWaiting = 0;
        timeToWait = Random.Range(minWaitTime, maxWaitTime);        
    }

    public void RestartTimeManual(float value)
    {
        timeSinceWaiting = 0;
        timeToWait = value;
    }

    public void StartMovement()
    {
        Turn();
        anim.SetBool("Move",true);
    }

    public void SetDestination(Vector3Int v3)
    {
        destination = v3;
    }

    public void RandomDirection()
    {
        int r = Random.Range(0,4);
        switch (r)
        {
            case 0:
            {
                direction = Directions.Front;
            }
            break;

            case 1:
            {
                direction = Directions.Back;
            }
            break;

            case 2:
            {
                direction = Directions.Left;
            }
            break;

            case 3:
            {
                direction = Directions.Right;
            }
            break;
        }
    }

    public bool CanMove(Directions d)
    {
        Vector3Int destiny = new Vector3Int(x,y,z);
        switch (d)
        {
            case Directions.Front:
            {
                destiny = new Vector3Int(x,y,z+1);

                //if (destiny.z - initialPos.z < distanceLimit)
                //{            
                //    return false;
                //}
            }
            break;

            case Directions.Back:
            {
                destiny = new Vector3Int(x,y,z-1);
                if (destiny.z < 0)// || initialPos.z - destiny.z < distanceLimit)
                {
                    return false;
                }
            }
            break;

            case Directions.Right:
            {
                destiny = new Vector3Int(x+1,y,z);
                if (destiny.x > 6)
                {
                    return false;
                }
            }
            break;

            case Directions.Left:
            {
                destiny = new Vector3Int(x-1,y,z);
                if (destiny.x < 0)
                {
                    return false;
                }
            }
            break;
        }

        if (!FindObstacleList(new ObstacleType[]{ObstacleType.Stone,
        ObstacleType.EmeraldOre, ObstacleType.BrakedStone, ObstacleType.MovableStone,
        ObstacleType.MineCart, ObstacleType.Spikes, ObstacleType.IceBlock, ObstacleType.Lava}, 
        destiny.x,destiny.y,destiny.z,true) && !FindObstacleList(new ObstacleType[]{ObstacleType.Lava, 
        ObstacleType.IceBlock}, destiny.x, destiny.y-1,destiny.z, true))
        {
            if (y > 0)
            {
                if (FindObstacleList(new ObstacleType[]{ObstacleType.Stone, //ObstacleType.EmeraldOre,
                },//ObstacleType.BrakedStone}, 
                destiny.x, destiny.y - 1, destiny.z, true))
                {
                    SetDestination(destiny);
                    return true;
                }
                else
                {
                    return false;
                }
            }

            else
            {
                SetDestination(destiny);
                return true;                
            }
        }

        return false;
    }

    public bool CanMoveSpot(Vector3Int spot)
    {
        if (!FindObstacleList(new ObstacleType[]{ObstacleType.Stone,
        ObstacleType.EmeraldOre, ObstacleType.BrakedStone, ObstacleType.MovableStone,
        ObstacleType.MineCart, ObstacleType.Spikes, ObstacleType.IceBlock, ObstacleType.Lava}, 
        spot.x,spot.y,spot.z,true))
        {
            if (spot.y > 0)
            {
                if (FindObstacleList(new ObstacleType[]{ObstacleType.Stone, ObstacleType.EmeraldOre,
                ObstacleType.BrakedStone}, 
                spot.x, spot.y - 1, spot.z, true))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }

            else
            {
                return true;                
            }
        }
        return false;
    }

    public bool CanFollow()
    {
        if (Vector3.Distance(transform.position, playerTransform.position) < followRange)
        {
            print("Posso seguir");
            return true;
        }

        else
        {
            print("Não posso seguir");
            return false;
        }
    }

    public Vector3Int EmptyHorizontal()
    {
        Vector3Int hor = Vector3Int.zero;
        switch(horizontalDirection)
        {
            case Directions.Left:
            {
                hor = new Vector3Int(-1,0,0);
            }
            break;

            case Directions.Right:
            {
                hor = new Vector3Int(1,0,0);
            }
            break;
        }

        return hor;
    }

    public Vector3Int EmptyVertical()
    {
        Vector3Int ver = Vector3Int.zero;
        switch(verticalDirection)
        {
            case Directions.Front:
            {
                ver = new Vector3Int(0,0,1);
            }
            break;

            case Directions.Back:
            {
                ver = new Vector3Int(0,0,-1);
            }
            break;
        }

        return ver;
    }

    public bool HasStraightPath(Directions d)
    {        
        bool horizontal = false;

        if (d == Directions.Right || d == Directions.Left)
        {
            horizontal = true;
        }

        Vector3Int checkPos = new Vector3Int(x,y,z);
        if (horizontal)
        {
            int playerX = (int)playerTransform.position.x;
            if (horizontalDirection == Directions.Left)
            {
                for (int i = x; i > playerX; i--)
                {
                    checkPos.x = i;
                    if (!CanMoveSpot(checkPos))
                    {
                        return false;
                    }
                } 
            }

            else
            {
                for (int i = x; i < playerX; i++)
                {
                    checkPos.x = i;
                    if (!CanMoveSpot(checkPos))
                    {
                        return false;
                    }
                } 
            }      
        }   

        else
        {
            int playerZ = (int)playerTransform.position.z;
            if (verticalDirection == Directions.Front)
            {
                for (int i = z; i < playerZ; i++)
                {
                    checkPos.z = i;
                    {
                        if (!CanMoveSpot(checkPos))
                        {
                            return false;
                        }
                    }
                }
            }

            else
            {
                for (int i = z; i > playerZ; i--)
                {
                    checkPos.z = i;
                    {
                        if (!CanMoveSpot(checkPos))
                        {
                            return false;
                        }
                    }
                }
            }
        } 

        return true;
    }

    public bool CanMoveToFutureDestiny()
    {
       Vector3Int ver = EmptyVertical(); 
       Vector3Int hor = EmptyHorizontal();

       Vector3Int firstDestiny = Vector3Int.zero, secondDestiny = Vector3Int.zero;
       Directions tmpD = GetPriority();

       if (tmpD == Directions.Front || tmpD == Directions.Back)
       {
           firstDestiny = new Vector3Int(x,y,z) + ver;
           secondDestiny = firstDestiny + hor;
       }

       else
       {
           firstDestiny = new Vector3Int(x,y,z) + hor;
           secondDestiny = firstDestiny + ver;
           print ("TMP D:" + tmpD +" FD:"+ firstDestiny +"SD: "+secondDestiny);
       }

       if (!CanMoveSpot(firstDestiny))
       {
           return false;
       }

       else
       {
           if (x < 1 || x > 5)
           {
               horizontalDirection = (Directions)(-(int)horizontalDirection);
           }

           if (CanMoveSpot(secondDestiny))
           {
               //Teste               
               return true;
           }
       }

       return false;
    }

    public bool MakePath()
    {
        bool found = false;
        bool right = false;
        Vector3Int checkPos = new Vector3Int(x,y,z);

        // Definindo a direção da checagem
        if (x > 3)
            {
                right = true;
            }
            
        else if (x < 3)
            {
                right = false;
            }

        else
            {
                if (horizontalDirection == Directions.Right)
                {
                    right = true;
                }

                else
                {
                    right = false;
                }
            }

        if (verticalDirection == Directions.Front)
        {
            checkPos.z += 1;
        }

        else if (verticalDirection == Directions.Back)
        {
            checkPos.z -= 1;
        }

        if (right)
        {    
            for (int i = checkPos.x; i < 7; i++)
            {
                checkPos.x = i;
                if (CanMoveSpot(checkPos) && !found)
                {
                    alternatePath = checkPos;
                    found = true;
                }
            }
            
            if (!found)
            {
                for (int i = checkPos.x; i > -1; i--)
                {
                    checkPos.x = i;
                    if (CanMoveSpot(checkPos) || !found)
                    {
                        alternatePath = checkPos;
                        found = true;
                    }
                }
            }
        }
        else
        {
            for (int i = checkPos.x; i > -1; i--)
            {
                checkPos.x = i;
                if (CanMoveSpot(checkPos))
                {
                    alternatePath = checkPos;
                    found = true;
                }
            }

            if (!found)
            {
                for (int i = checkPos.x; i < 7; i++)
                {
                    checkPos.x = i;
                    if (CanMoveSpot(checkPos))
                    {
                        alternatePath = checkPos;      
                        found = true;
                    }
                }
            }
        }        
        return found;
    }

    public void GetFollowDirections(bool player = true)
    {
        GetPosition();
        zeroHorizontal = false;
        zeroVertical = false;

        Vector3 path = Vector3.zero;

        if (player)
        {
            path = playerTransform.position - transform.position;
        }

        else
        {
            path = alternatePath - transform.position;   
        }

        playerDistance = new Vector3Int((int)path.x, (int)path.y, (int)path.z);

        print(playerDistance);
        {
            // Horizontal
            if (playerDistance.x > 0)
            {
                horizontalDirection = Directions.Right;
            }

            else if (playerDistance.x < 0)
            {
                horizontalDirection = Directions.Left;
            }

            else
            {
                zeroHorizontal = true;
            }

            // Vertical
            if (playerDistance.z > 0)
            {
                verticalDirection = Directions.Front;
            }

            else if (playerDistance.z < 0)
            {
                verticalDirection = Directions.Back;
            }

            else
            {
                zeroVertical = true;
            }
        }

    }

    public Directions GetPriority()
    {
        if (verticalDirection == Directions.Front)
        {
            if (CanMove(Directions.Front))
            {
                if (zeroVertical)
                {
                    return horizontalDirection;
                }

                else
                {
                    return verticalDirection;
                }
            }

            else
            {
                return horizontalDirection;
            }
        }   

        else
        {
            if (zeroHorizontal)
            {
                return verticalDirection;
            }

            else
            {
                return horizontalDirection;
            }
        }
    }

    public void DecideMovement()
    {    
        print ("DECIDE");
        if (CanFollow())
        {            
            GetFollowDirections();
            direction = GetPriority();
            print(direction);
            if (Vector3.Distance(transform.position, playerTransform.position) > 1)
            {
                if (!HasStraightPath(direction) && zeroHorizontal ||!HasStraightPath(direction) && zeroVertical)
                {
                    if (!CanMoveToFutureDestiny())
                    {
                        print("NÃO PODE SE MOVER");
                        MakePath();
                        GetFollowDirections(false);
                        altPath = true;
                        direction = GetPriority();
                    }
                }     
            }
        }
            //move = CanMove();   
        else
        {
            bool move = false;
            RandomDirection();

            while (!move)
            {
                RandomDirection();
                move = CanMove(direction);
            }            
        }
    

        if (AttackRange())
        {
            if (playerTransform.position.y == y && canKill)
            StartAttack();            
        }

        else
        {
            if (!CanMove(direction))
            {
                print("Não vai acontecer");
                RestartTime();
                return;
            }
            StartMovement();   
        }
    }

    public void Turn()
    {
        print("TURN!!!!!!");
        switch(direction)
        {
            case Directions.Front:
            {
                transform.localEulerAngles = new Vector3(transform.rotation.x,0,transform.rotation.z);//new Quaternion(transform.localRotation.x,0,transform.localRotation.z, 0);
            }
            break;

            case Directions.Back:
            {
                transform.localEulerAngles = new Vector3(transform.localRotation.x,180,transform.localRotation.z);
                //transform.rotation = new Quaternion(transform.localRotation.x,180,transform.localRotation.z, 0);
            }
            break;

            case Directions.Left:
            {
                transform.localEulerAngles = new Vector3(transform.localRotation.x,270,transform.localRotation.z);
                //transform.rotation = new Quaternion(transform.localRotation.x,270,transform.localRotation.z, 0);
            }
            break;

            case Directions.Right:
            {
                transform.localEulerAngles = new Vector3(transform.localRotation.x,90,transform.localRotation.z);
                //transform.rotation = new Quaternion(transform.localRotation.x,90,transform.localRotation.z, 0);
            }
            break;
        }
    }
    public void GetPosition()
    {
        x = (int)transform.position.x;
        y = (int)transform.position.y;
        z = (int)transform.position.z;
    }

    public void SecondCheck()
    {
        GetPosition();
    }

    public bool AttackRange()
    {
        if (zeroHorizontal || zeroVertical)
        {
            if (Vector3.Distance(transform.position, playerTransform.position) <= 1)
            {
                print("Perto");
                return true;
            }
        }

        return false;
    }

    public void StartAttack()
    {
        Turn();
        RestartTimeManual(attackTime);
        anim.SetTrigger("Attack");
        moving = false;
    }

    public void CollisionCheck()
    {
        if (transform.position.z < lavaWallTransform.position.z)
        {
            print("LAVA");
            if (transform.position.y == lavaWallTransform.position.y)
            {               
                Die(true);
            }
        }

        if (FindObstacle(ObstacleType.MovableStone,x,y,z, true))
        {
            Die();            
        }

        if (canKill)
        {
            if (Vector3.Distance(transform.position, playerTransform.position) < 0.5f)
            {
                if (playerTransform.position.y == transform.position.y)
                {
                    DefeatPlayer();
                }
            }
        }
    }

    [ContextMenu("DIE")]
    public void Die(bool lava = false)
    {
        if (enabled)
        {
            if (lava)
            {
                lavaParticle.Emit(10);
                sfx.PlayClip("LavaHit");
            }

            else
            {
                deathParticle.Emit(10);
            }

            insectNoise.audioSource.loop = false;
            insectNoise.PlayClip("Death");

            moving = false;
            canKill = false;
            anim.SetTrigger("Die");
            Destroy(gameObject,2);
            enabled = false;
        }
    }

    // ANIMATION EVENT

    public void AttackCollision()
    {
        Vector3 attackPosition = transform.position;        
        if (direction == Directions.Left || direction == Directions.Right)
        {
            attackPosition += EmptyHorizontal();
        }

        else
        {
            attackPosition += EmptyVertical();
        }

        if (Vector3.Distance(attackPosition, transform.position)< 1.2f)
        {
            DefeatPlayer();
        }

        else
        {
            sfx.PlayClip("AttackMiss");
        }
    }

    public void DefeatPlayer()
    {
        playerTransform.gameObject.GetComponent<PlayerMovement>().Die();
        canKill = false;
        sfx.PlayClip("AttackHit");
    }

    public void Move()
    {
        movePercentage = 0;
        sfx.PlayClip("Move");
        //currentSpeed = Random.Range(minSpeed, maxSpeed);
        moving = true;
    }
    ///////

    public bool FindObstacle(ObstacleType ob, int x, int y, int z, bool checkHeight)
    {
        if (x > 6 || x < 0 || z < 0)
        {
            return false;
        }

        if (lvlCoord.FindObstacle(ob, x, y, z, checkHeight))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public bool FindObstacleList(ObstacleType[] ob, int x, int y, int z, bool checkHeight)
    {
        if (x > 6 || x < 0 || z < 0)
        {
            return false;
        }

        if (lvlCoord.FindObstacleList(ob, x, y, z, checkHeight))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
