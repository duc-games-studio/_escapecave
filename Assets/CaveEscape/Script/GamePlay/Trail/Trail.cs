﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trail : MonoBehaviour
{
    public TrailType trailType;
    public TrailType inverseTrailType;

    public void SetInverseType(TrailType type)
    {
        inverseTrailType = type;
    }
}
