﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Minecart : MonoBehaviour
{
    public Directions currentDirection;

    bool forward;
    bool up;
    bool down;

    public bool moving;

    bool inverse = false;

    public float speed = 8;

    public Transform minecartModel;


    public int x, y, z;

    public Vector3Int destination;

    AudioSource aSource;

    PlayerMovement tmpPlayer;

    private void Start()
    {
        GetPosition();

        //moving = false;

        aSource = GetComponent<AudioSource>();
    }

    private void FixedUpdate()
    {
        

        if (moving)
        {            
            transform.position = Vector3.MoveTowards(transform.position, destination, speed * Time.deltaTime);

            if (!aSource.isPlaying)
            {
                aSource.Play();
            }

            if (transform.position == destination)
            {
                SetNextMove();
            }
        }

        else
        {
            EndMove();
        }

    }

    public void GetPosition()
    {
        x = (int)transform.position.x;
        y = (int)transform.position.y;
        z = (int)transform.position.z;
    }

    public void SetDestination(int x, int y, int z)
    {
        destination = new Vector3Int(x, y, z);
    }

    public void SetNextMove()
    {
        print("Aconteceu");
        GetPosition();

        tmpPlayer.FindCollectable(x,y,z);

        if (FindTrail(TrailType.BrokenTrail))
        {
            EndMove();
            return;
        }

        /*if (FindTrail(TrailType.ForwardTrail))
        {
            
            
        }*/




        /*if (FindTrail(TrailType.ForwardToRightTrail))
        {
            switch (currentDirection)
            {
                case Directions.Front:
                    {
                        SetDirection(Directions.Right);
                    }
                    break;

                case Directions.Back:
                    {
                        print("Viraraesquerda");
                        SetDirection(Directions.Right);
                    }
                    break;

                case Directions.Left:
                    {
                        SetDirection(Directions.Back);
                    }
                    break;

                case Directions.Right:
                    {
                        SetDirection(Directions.Right);
                    }
                    break;
            }

        }

        else if (FindTrail(TrailType.ForwardToLeftTrail))
        {
            switch (currentDirection)
            {
                case Directions.Front:
                    {
                        SetDirection(Directions.Left);
                    }
                    break;

                case Directions.Back:
                    {
                        print("Viraradireita");
                        SetDirection(Directions.Left);
                    }
                    break;

                case Directions.Left:
                    {
                        SetDirection(Directions.Back);
                    }
                    break;

                case Directions.Right:
                    {
                        SetDirection(Directions.Front);
                    }
                    break;
            }

        }*/

        if (FindTrail(TrailType.ToFrontTrail))
        {
            SetDirection(Directions.Front);
        }

        else if (FindTrail(TrailType.ToBackTrail))
        {
            SetDirection(Directions.Back);
        }

        else if (FindTrail(TrailType.ToLeftTrail))
        {
            SetDirection(Directions.Left);
        }

        else if (FindTrail(TrailType.ToRightTrail))
        {
            SetDirection(Directions.Right);
        }



        switch (currentDirection)
        {
            case Directions.Front:
                {
                    SetDestination(x, y, z + 1);
                }
                break;

            case Directions.Back:
                {
                    SetDestination(x, y, z - 1);

                    if (z - 1 < 0)
                    {
                        EndMove();
                        return;
                    }
                }
                break;

            case Directions.Left:
                {
                    SetDestination(x - 1, y, z);

                    if (x - 1 < 0)
                    {
                        EndMove();
                        return;
                    }
                }
                break;

            case Directions.Right:
                {
                    SetDestination(x + 1, y, z);

                    if (x + 1 > GameManager.instance.lvlCoordinates.lvlWidth)
                    {
                        EndMove();
                        return;
                    }
                }
                break;
        }


        if (tmpPlayer.FindObstacle(ObstacleType.MovableStone, destination.x, destination.y, destination.z, true))
        {
            print("Pedra movível no caminho");
            EndMove();
            return;
        }


        if (HasTrail(destination.x, destination.y, destination.z))
        {
            moving = true;
        }

        else
        {
            EndMove();
        }
    }


    public void EndMove()
    {
        tmpPlayer.EndRide();
        print("Acabado");
        moving = false;
        aSource.Stop();
        enabled = false;
    }

    public void Inverse()
    {
        SetDirection((Directions)(-(int)currentDirection));

        inverse = !inverse;
    }

    public void SetDirection(Directions d)
    {
        currentDirection = d;

        switch (currentDirection)
        {
            case Directions.Front:
                {
                    transform.localRotation = Quaternion.Euler(0, 0, 0);
                }
                break;

            case Directions.Back:
                {
                    transform.localRotation = Quaternion.Euler(0, 180, 0);
                }
                break;

            case Directions.Left:
                {
                    transform.localRotation = Quaternion.Euler(0, -90, 0);
                }
                break;

            case Directions.Right:
                {
                    transform.localRotation = Quaternion.Euler(0, 90, 0);
                }
                break;
        }
    }

    ///// MÉTODOS DA MATRIZ \\\\\
    
    public bool FindTrail(TrailType type)
    {
        if (GameManager.instance.lvlCoordinates.FindTrail(type, x,y,z, inverse))
        {
            return true;
        }

        return false;
    }

    public bool HasTrail(int x, int y, int z)
    {
        if (GameManager.instance.lvlCoordinates.FindTag("Trail", x, y, z, true))
        {
            print("Tem Trilho");
            return true;
        }

        return false;
    }



    public void SetSpeed(float value)
    {
        speed = value;
    }

    public void GetPlayer()
    {
        tmpPlayer = FindObjectOfType<PlayerMovement>();
    }
}
