﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovableStone : MonoBehaviour
{
    public bool canPush;

    public bool moving;
    public bool falling;

    public bool sliding;

    public Vector3Int destination;

    int x, y, z;
    public int pX, pZ; // Previous
        
    public Directions moveDirection = Directions.Front;

    public float speed;

    public float slideSpeed;

    public float fallSpeed;

    float moveSpeed;

    LevelCoordinates tmpCoord;

    public SFX stoneSFX;
    public GameObject lavaParticle;
    public GameObject groundParticle;

    private void Start()
    {
        moveSpeed = speed;

        tmpCoord = FindObjectOfType<LevelCoordinates>();

        GetPosition();

        destination = new Vector3Int(x, y, z);
    }

    private void FixedUpdate()
    {
        if (moving)
        {
            if (sliding)
            {
                transform.position = Vector3.MoveTowards(transform.position, destination, Time.deltaTime * slideSpeed);
            }

            else
            {
                transform.position = Vector3.MoveTowards(transform.position, destination, Time.deltaTime * moveSpeed);
            }

            if (transform.position == destination)
            {
                if (!stoneSFX.audioSource.isPlaying)
                {
                    stoneSFX.PlayClip("Move");
                }

                transform.position = destination;
                RemoveFromMatrix(pX, pZ);
                GetPosition();
                AddToMatrix();
                SecondCheck();
                print("Check");

                if (!sliding)
                {
                    moving = false;
                }
                
            }
        }

        if (falling)
        {
            transform.position = Vector3.MoveTowards(transform.position, destination, Time.deltaTime * fallSpeed);

            if (transform.position == destination)
            {
                transform.position = destination;
                RemoveFromMatrix(pX, pZ);
                GetPosition();
                AddToMatrix();
                SecondCheck();
            }
        }
    }


    public void GetDirection(Directions direction)
    {

        moveDirection = direction;
        SetDestination();

    }

    public void GetPosition()
    {
        x = (int)transform.position.x;
        y = (int)transform.position.y;
        z = (int)transform.position.z;
    }

    public void SetDestination()
    {

        GetPosition();
        pX = x;
        pZ = z;

        if (falling)
        {
            destination = new Vector3Int(x, y - 1, z);
            return;
        }

        switch(moveDirection)
        {

            case Directions.Front:
                {
                    destination = new Vector3Int(x, y, z + 1);
                }
                break;

            case Directions.Back:
                {
                    destination = new Vector3Int(x, y, z - 1);
                }
                break;

            case Directions.Left:
                {
                    destination = new Vector3Int(x - 1, y, z);
                }
                break;

            case Directions.Right:
                {
                    destination = new Vector3Int(x + 1, y, z);
                }
                break;
        }
    }

    [ContextMenu("SetAction")]
    public void SetAction()
    {
        if (destination.x > tmpCoord.lvlWidth || destination.x < 0 || destination.z < 0)
        {
            CanPush(false);
            return;
        }

        else
        {
            if (FindObstacleList(new ObstacleType[] {ObstacleType.Stone,
            ObstacleType.MovableStone, ObstacleType.EmeraldOre,
            ObstacleType.BrakedStone, ObstacleType.Spikes}, destination.x, destination.y, destination.z)
            || FindObstacle(ObstacleType.Lava, x,y,z)
            || FindCollectable(destination.x, destination.y, destination.z))
            {
                CanPush(false);
            }

            else
            {
                CanPush(true);
            }
        }
        
    }

    public void CanPush(bool value)
    {
        canPush = value;
    }

    public void SecondCheck()
    {
        print("SECOND CHECK 1");
        //RemoveFromMatrix(pX, pZ);
        //GetPosition();
        //AddToMatrix();


        /*if (sliding)
        {
            RemoveFromMatrix(gameObject);

            AddToMatrix();
        }*/

        if (y > 0)
        {
            if (FindObstacleList(new ObstacleType[] { ObstacleType.Stone,
                ObstacleType.MovableStone,
                ObstacleType.EmeraldOre, ObstacleType.BrakedStone}, x, y - 1, z))
            {
                if (falling)
                {
                    print("Acabou a queda");

                    if (FindObstacle(ObstacleType.Lava, x, y, z))
                    {
                        stoneSFX.PlayClip("HitLava");
                        Instantiate(lavaParticle, transform.position, transform.rotation);
                    }

                    else
                    {
                        stoneSFX.PlayClip("HitGround");
                    }
                }

                CanPush(true);
                falling = false;

                //enabled = false;
                //StartFalling();


                return;
            }

            else
            {
                //enabled = true;
                CanPush(false);
                StartFalling();
                //falling = false;
            }
        }
       
        else
        {
            if (falling)
            {
                print("Acabou a queda");

                if (FindObstacle(ObstacleType.Lava, x, y, z))
                {
                    stoneSFX.PlayClip("HitLava");
                    Instantiate(lavaParticle, transform.position, transform.rotation);
                }

                else
                {
                    stoneSFX.PlayClip("HitGround");
                }
            }

            CanPush(true);
            falling = false;

            //enabled = false;
            //return;

        }

        if (sliding)
        {
            SetDestination();

            if (FindObstacleList(new ObstacleType[] {ObstacleType.Stone,
                ObstacleType.EmeraldOre, ObstacleType.BrakedStone,
                ObstacleType.MovableStone}, destination.x, destination.y, destination.z))
            {
                stoneSFX.PlayClip("HitGround");
                stoneSFX.audioSource.loop = false;

                sliding = false;
                moving = false;

                //enabled = false;

                return;
            }

        }

        if (FindObstacle(ObstacleType.IceBlock, x, y - 1, z))
        {
            if (!sliding)
            {
                RemoveFromMatrix(x, z);
            }
            print("AchouGelo");
            StartSlide();
            return;
        }

        else
        {
            print("Acabaste?");
            if (sliding)
            {
                sliding = false;
                moving = false;
                stoneSFX.audioSource.Stop();
                stoneSFX.audioSource.loop = false;
            }

            //enabled = false;
        }


    }

    public void StartFalling()
    {
        print("Começou a queda");
        RemoveFromMatrix(pX, pZ);
        falling = true;
        SetDestination();
    }

    [ContextMenu("StartMove")]
    public void StartMove()
    {
        //RemoveFromMatrix(pX, pZ);
        //RemoveFromMatrix();
        CanPush(false);
        moving = true;

        stoneSFX.PlayClip("Move");
    }

    public void StartSlide()
    {
        //RemoveFromMatrix();





        //tmpStone = gameObject;

        //if (tmpStone == null)
        //{
        //    
        //}

        //else
        //{
        //    RemoveFromMatrix(tmpStone);
        //}

        //tmpStone = AddToMatrix();

        //tmpStone = AddToMatrix();

        //SetDestination();

        //if (sliding)
        //{
        //    RemoveFromMatrix(gameObject, previousDestination.x, previousDestination.z);
        //}

        //else
        //{
        //    RemoveFromMatrix(gameObject, x, z);
        //}

        CanPush(false);

        if (!sliding)
        {
            stoneSFX.PlayClip("Slide");
            stoneSFX.audioSource.loop = true;
        }


        sliding = true;
        moving = true;


        
    }

    public void GetSpeed(float value)
    {
        if (FindObstacle(ObstacleType.IceBlock, x,y-1,z))
        {
            moveSpeed = slideSpeed;
        }

        else
        {
            moveSpeed = value;
        }
        
    }

    /*public void Slide()
    {
        SetDestination();

        moving = true;
        sliding = true;

        //RemoveFromMatrix();
        CanPush(false);
        
    }*/



    /*public void GetPreviousPosition()
    {        

        switch (moveDirection)
        {

            case Directions.Front:
                {
                    previousDestination = new Vector3Int(x, y, z - 1);
                }
                break;

            case Directions.Back:
                {
                    previousDestination = new Vector3Int(x, y, z + 1);
                }
                break;

            case Directions.Left:
                {
                    previousDestination = new Vector3Int(x + 1, y, z);
                }
                break;

            case Directions.Right:
                {
                    previousDestination = new Vector3Int(x - 1, y, z);
                }
                break;
        }
    }*/

    // Métodos da "Matriz 3D"

    public bool FindObstacle(ObstacleType obstacleType ,int x, int y, int z)
    {
        if (tmpCoord.FindObstacle(obstacleType, x,y,z, true))
        {
            return true;
        }

        return false;
    }

    public bool FindObstacleList(ObstacleType[] obstacleType, int x, int y, int z)
    {
        if (tmpCoord.FindObstacleList(obstacleType, x, y, z, true))
        {
            return true;
        }

        return false;
    }

    public bool FindCollectable(int x, int y, int z)
    {
        if (tmpCoord.FindCollectable(x,y,z, false))
        {
            return true;
        }
        return false;
    }

    [ContextMenu("RemoveFromMatrix")]
    public void RemoveFromMatrix(int x, int z)
    {
        print("REMOVER");
        tmpCoord.RemoveUnit(this.gameObject, x, z);
    }

    public void AddToMatrix()
    {
        print("ADICIONAR");
         tmpCoord.AddToMatrix(gameObject, UnitType.Obstacle);
    }
}
