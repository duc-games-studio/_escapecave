﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// SCRIPT PARA SER COLOCADO NOS OBJETOS QUE SÃO OBSTÁCULOS E ADICIONADOS NA "MATRIZ 3D"
public class Obstacle : MonoBehaviour
{
    [Header("Tipo de obstáculo")]
    public ObstacleType obstacleType;

    [ContextMenu("AddToMatrix")]
    public void AddToMatrix()
    {
         FindObjectOfType<LevelCoordinates>().AddToMatrix(this.gameObject, UnitType.Obstacle);
    }
}
