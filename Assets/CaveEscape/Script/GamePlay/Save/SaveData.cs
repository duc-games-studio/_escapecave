using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class SaveData
{
    //public int completedStages;
    public int diamondAmount, rubyAmount;

    public StagePersistent.StageData[] stageData = new StagePersistent.StageData[3];

    public SaveData(SaveData data)
    {
        //completedStages = data.completedStages;//stPersist.completedStages;
        diamondAmount = data.diamondAmount;//stPersist.savedDiamonds;
        rubyAmount = data.rubyAmount;//stPersist.savedRubies;

        for (int i = 0; i < stageData.Length; i++)
        {
            stageData[i] = data.stageData[i];
        }

    }
}
