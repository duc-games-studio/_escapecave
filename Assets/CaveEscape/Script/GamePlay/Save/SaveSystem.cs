using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public static class SaveSystem
{
    public static void Save(SaveData sData)
    {
        string path, pathFile;
        path = Path.Combine(Application.persistentDataPath, "CaveSave");
        //path = Path.Combine(path, "Savedata.txt");
        pathFile = Path.Combine(path, "Savedata.cavedata");
        Debug.Log(path);
        Debug.Log(pathFile);
        if (!Directory.Exists(Path.Combine(Application.persistentDataPath, "CaveSave")))
        {
            Directory.CreateDirectory(Path.Combine(Application.persistentDataPath, "CaveSave"));
            Debug.Log("Created?");
        }

        // BIN�RIO

        BinaryFormatter formatter = new BinaryFormatter();
        FileStream stream;
        stream = new FileStream(pathFile, FileMode.Create, FileAccess.ReadWrite);
        SaveData data = new SaveData(sData);
        formatter.Serialize(stream, sData);
        stream.Close();

        /*if (Application.platform == RuntimePlatform.Android)
        {
            path = "/storage/emulated/0/";
        }

        else
        {
            path = Application.dataPath + "/";
        }*/



        //string folderName = "CaveSave";


        //string jsonString = JsonUtility.ToJson(sData);
        //Debug.Log(jsonString);

        
        //File.WriteAllText((path + folderName) + "/SaveData.txt", jsonString);
        //File.WriteAllText(path, jsonString);
    }

    public static SaveData Load()
    {
        if (!Directory.Exists(Path.Combine(Application.persistentDataPath, "CaveSave")))
        {
            Directory.CreateDirectory(Path.Combine(Application.persistentDataPath, "CaveSave"));
            Debug.Log("Created?");
        }

        string path, pathFile;
        path = Path.Combine(Application.persistentDataPath, "CaveSave");
        //path = Path.Combine(path, "Savedata.txt");
        pathFile = Path.Combine(path, "Savedata.cavedata");
        Debug.Log(path);
        Debug.Log(pathFile);
        /*if (Application.platform == RuntimePlatform.Android)
        {
            path = "/storage/emulated/0/";
        }

        else
        {
            path = Application.dataPath + "/";
        }*/

        //string folderName = "CaveSave";

        //Debug.Log(path + folderName);
        /*if (File.Exists(path))
        {
            string saveString = File.ReadAllText(path);
            return JsonUtility.FromJson<SaveData>(saveString);
        }

        else
        {
            Debug.Log("No Save file in " + (path));
            return null;
        }*/
        //string path = Application.persistentDataPath + "/SaveData.CAVEDATA";
        //Debug.Log(path);

        if (File.Exists(pathFile))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(pathFile, FileMode.Open);
            stream.Position = 0;
            SaveData data = formatter.Deserialize(stream) as SaveData;
            //Debug.Log(data);
            stream.Close();
            return data;
        }

        else
        {
            Debug.Log("No Save file in " + path);
            return null;
        }

    }
}
