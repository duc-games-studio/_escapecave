﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleScript : MonoBehaviour
{
    public ParticleSystem pSystem;

    public int emissionAmount;

    private void Start()
    {
        pSystem.Emit(emissionAmount);
        Destroy(gameObject, pSystem.main.startLifetime.constant);
    }
}
