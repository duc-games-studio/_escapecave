using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerItem : MonoBehaviour
{
    ///// CONTROLES \\\\\
    public PlayerControls controls;

    private void OnEnable()
    {
        controls.Enable();
    }

    private void OnDisable()
    {
        controls.Disable();
    }

    private void Awake()
    {
        controls = new PlayerControls();
        controls.Miner.MinerItem.performed += ctx => UseItem(true);
        controls.Miner.MinerItem.canceled += ctx => UseItem(false);
    }

    // Objetos
    public GameObject pickaxe;

    public Collectable item;
    public Obstacle obstacle;

    // Personagem

    public PlayerMovement pScript;

    [HideInInspector]
    public bool holdingButton, hasItem, destroyBlock;

    HUDManager hudM;

    Ladybug[] enemies;

    private void Start()
    {
        pickaxe.SetActive(false);
        hudM = GameManager.instance.hudManager;

        enemies = FindObjectsOfType<Ladybug>();
    }

    private void FixedUpdate()
    {
        if (pScript.pState == PlayerState.Interacting && holdingButton)
        {
            ReceiveDirection();
            hudM.UpdateArrowOrientation(pScript.moveDirection);
        }
    }

    public void ReceiveDirection()
    {
        if (pScript.enabled)
        pScript.Orientate();
    }

    public void UseItem(bool button)
    {
        if (!pScript.enabled) return;
        print("UseItem");
        print(button);
        if (hasItem)
        {
            holdingButton = button;
            if (pScript.pState == PlayerState.Standing)
            {
                if (holdingButton)
                {
                    hudM.UpdateArrowOrientation(pScript.moveDirection);
                    hudM.ShowItemArrow(true);
                    pScript.pState = PlayerState.Interacting;
                }
            }
                //public void MinePlay()
                //{
            if (pScript.pState == PlayerState.Interacting)
            {
                if (holdingButton)
                {
                    print("Mine");
                    StartMining();
                }

                else
                {
                    print("EndMine");
                    ReleaseMining();
                    hudM.ShowItemArrow(false);
                }
                //TryMining();
            } 
        }        
    }

    public void StartMining()
    {
        pScript.anim.SetBool("Mine", true);
    }

    public void ReleaseMining()
    {
        pScript.anim.SetBool("Mine", false);
    }

    public void TryMining()
    {        
        //GetPlayerPosition();

        //print("Criando dire��o");
        //switch (pScript.moveDirection)
        //{
            /*case Directions.Front:
                {
                    SetDestination(new Vector3Int(playerX, playerY, playerZ + 1), false);
                }
                break;

            case Directions.Back:
                {
                    SetDestination(new Vector3Int(playerX, playerY, playerZ - 1), false);
                }
                break;

            case Directions.Left:
                {
                    SetDestination(new Vector3Int(playerX - 1, playerY, playerZ), false);
                }
                break;

            case Directions.Right:
                {
                    SetDestination(new Vector3Int(playerX + 1, playerY, playerZ), false);
                }
                break;
        }

        if (destination.x > lvlWidth || destination.x < 0 || destination.z < 0)
        {
            print("N�o d� pra minerar");
            return;
        }

        else
        {
            if (FindObstacle(ObstacleType.EmeraldOre, destination.x, destination.y, destination.z, true))
            {
                print("Bloco de Diamante");
                miningBlock = ObstacleType.EmeraldOre;
                tmpBlock = ReturnObstacle(ObstacleType.EmeraldOre, destination.x, destination.y, destination.z, true);
                UsePickaxe();
            }

            else if (FindObstacle(ObstacleType.BrakedStone, destination.x, destination.y, destination.z, true))
            {
                print("Pedra Rachada");
                miningBlock = ObstacleType.BrakedStone;
                tmpBlock = ReturnObstacle(ObstacleType.BrakedStone, destination.x, destination.y, destination.z, true);
                UsePickaxe();
            }*/
        //}
    }

    public Obstacle ReturnAnyObstacle(int x, int y, int z)
    {
        return GameManager.instance.lvlCoordinates.ReturnAnyObstacle(x, y, z);
    }

    public void GetPickaxe()
    {
        pickaxe.SetActive(true);
        hasItem = true;
    }

    public void UsePickaxe()
    {
        pScript.anim.SetTrigger("Mine");
        //CanMove(false);
    }
    public void PickaxeHit()
    {
        Vector3Int destination = GetDestination(pScript.moveDirection);
        destroyBlock = false;

        if (destination.x < 0 || destination.x > pScript.lvlWidth || destination.z < 0)
        {
            print("N�o vai acontecer.");
            return;
        }
         
        AttackCollision(destination);

        obstacle = ReturnAnyObstacle(destination.x, destination.y, destination.z);

        if (obstacle != null)
        {
            print("Minerar");

            switch (obstacle.obstacleType)
            {
                case ObstacleType.EmeraldOre:
                    {
                        pScript.diamondParticle.transform.position = obstacle.gameObject.transform.position;
                        pScript.diamondParticle.Emit(5);

                        int diamondAmount = Random.Range(1, 5);
                        GameManager.instance.score.AddDiamond(diamondAmount);
                        GameManager.instance.colEvents.InsertDiamonds(diamondAmount, obstacle.gameObject.transform.position, pScript.itemAudio);

                        destroyBlock = true;
                    }
                    break;

                case ObstacleType.BrakedStone:
                    {
                        destroyBlock = true;
                    }
                    break;
            }

            /*switch (obstacle.obstacleType)
        {
            case ObstacleType.EmeraldOre:
                {
                    Instantiate(pScript.part, tmpBlock.transform.position, tmpBlock.transform.rotation);

                    // colocando aqui pra n�o precisar editar evento em todas as fases
                    GameManager.instance.score.AddDiamond(3);

                    GameManager.instance.hudManager.StartCoroutine(GameManager.instance.hudManager.typeDiamondTXT());

                    itemAudio.PlayClip("Get");

                }
                break;

            case ObstacleType.BrakedStone:
                {
                    Instantiate(breakStoneParticle, tmpBlock.transform.position, tmpBlock.transform.rotation);


                }
                break;
        }*/

            if (destroyBlock)
            {
                pScript.minerAudio.PlayClip("PickaxeHit");

                GameManager.instance.lvlCoordinates.RemoveUnit(obstacle.gameObject, (int)obstacle.gameObject.transform.position.x, (int)obstacle.gameObject.transform.position.z);
                Destroy(obstacle.gameObject);
            }            
        }        
    }

    public Vector3Int GetDestination(Directions direction)
    {
        Vector3Int v3 = new Vector3Int();
        switch (direction)
        {
            case Directions.Front:
                {
                    v3 = new Vector3Int(pScript.playerX, pScript.playerY, pScript.playerZ + 1);
                }
                break;

            case Directions.Back:
                {
                    v3 = new Vector3Int(pScript.playerX, pScript.playerY, pScript.playerZ - 1);
                }
                break;

            case Directions.Left:
                {
                    v3 = new Vector3Int(pScript.playerX - 1, pScript.playerY, pScript.playerZ);
                }
                break;

            case Directions.Right:
                {
                    v3 = new Vector3Int(pScript.playerX + 1, pScript.playerY, pScript.playerZ);
                }
                break;
        }

        return v3;
    }

    public void AttackCollision(Vector3 attackSpot)
    {
        if (enemies.Length > 0)
        {
            for (int i = 0; i < enemies.Length; i++)
            {
                if (enemies[i] != null)
                {
                    if (Vector3.Distance(attackSpot, enemies[i].transform.position) < 1f)
                    {
                        pScript.minerAudio.PlayClip("PickaxeHit");
                        enemies[i].GetComponent<Ladybug>().Die();
                    }
                }     
            }
        }
    }
}
