﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem;


public class PlayerMovement : MonoBehaviour
{
    [Header("Esse script funciona como o gerenciador do personagem")]
    ///// CONTROLES \\\\\
    PlayerControls controls;
    bool holdingButton;
    public float hor, ver;

    ///// FLOAT \\\\\
    public float moveSpeed, fallSpeed, mudSpeed, movingStoneSpeed, slideSpeed;
    float standartSpeed, standartMovingStoneSpeed;

    ///// VETORES \\\\\
    public Vector3Int destination;

    ///// LARGURA DO CENÁRIO \\\\\
    public int lvlWidth;

    ///// POSIÇÃO DO PERSONAGEM \\\\\
    public int playerX, playerY, playerZ;

    // Bools de movimento
    [SerializeField]
    //private bool canMove;

    ///// BOOLS DE COLISÃO \\\\\
    bool hasStoneAhead, hasStoneAbove, hasStoneBelow, hasMud, hasLadders, hasLaddersAbove, canDetectLadders;
    Directions ladderDirection;

    ///// LISTA DE OBSTÁCULOS PRA COLISÃO \\\\\
    public List<Obstacle> obstacles = new List<Obstacle>();

    ///// COLETÁVEL TEMPORÁRIO \\\\\
    public CollectableType tmpColType;

    ///// DIREÇÃO \\\\\
    public Directions moveDirection;

    ///// ESTADO DO PERSONAGEM \\\\\
    public PlayerState pState = PlayerState.Standing;

    ///// PARTÍCULAS \\\\\
    public ParticleSystem lavaDeathParticles, spikeDeathParticles, mudParticle, diamondParticle, rubyParticle;

    ///// PEDRA MOVÍVEL \\\\\
    public MovableStone tmpMovableStone;
    public bool movingStone;

    ///// MINECART \\\\\
    public Minecart tmpMinecart;

    ///// EVENTOS \\\\\
    public UnityEvent deathEvent, endStageEvent;

    ///// CLASSES \\\\\
    public PlayerItem pItem;
    public Animator anim;
    public SFX minerAudio, minerVoice, itemAudio;
    //public PlayerAnimation pAnim; public PlayerItem pItem;// public PlayerSound pSound;
    /////ANIMADOR\\\\\
    public float slowAnimSpeed, normalAnimSpeed, fastAnimSpeed;
    bool rightStep;

    ///// LEVEL COORDINATES \\\\\
    LevelCoordinates lvlCoordinates;

    ///// GAMEOBJECT PAI ORIGINAL \\\\\
    Transform originalParent;

    private void OnEnable()
    {    
        controls.Enable();
        CheckInput();
    }

    private void OnDisable()
    {
        controls.Disable();
    }

    private void Awake()
    {
        controls = new PlayerControls();

        Keyboard key = Keyboard.current;
        if (key == null) return;

        controls.Miner.MinerMovement.performed += ctx => ReceiveInput(ctx.ReadValue<Vector2>());
        controls.Miner.TouchStick.performed += ctx => ReceiveInput(ctx.ReadValue<Vector2>());
    }


    private void Start()
    {
        lvlCoordinates = GameManager.instance.lvlCoordinates;

        lvlWidth = lvlCoordinates.lvlWidth;
        GetPlayerPosition();

        standartSpeed = moveSpeed;
        standartMovingStoneSpeed = movingStoneSpeed;
    }

    public void ReceiveInput(Vector2 vector)
    {
        if (!enabled) return;
        print("Recebeu");
        hor = vector.x;
        ver = vector.y;

        CheckInput();
    }

    public void CheckInput()
    {
        if (!enabled) return;
        if (hor != 0 || ver != 0)
        {
            holdingButton = true;
        }

        else
        {
            holdingButton = false;
        }

        if (holdingButton)
        {
            if (pState == PlayerState.Standing || pState == PlayerState.Interacting)
            {
                float tmpV = ver;
                float tmpH = hor;
                if (tmpV < 0)
                {
                    tmpV *= -1;
                }

                if (tmpH < 0)
                {
                    tmpH *= -1;
                }

                if (tmpV > tmpH)
                {
                    if (ver > 0)
                    {
                        moveDirection = Directions.Front;
                    }
                    else if (ver < 0)
                    {
                        moveDirection = Directions.Back;
                    }
                }
                
                else
                {
                    if (hor > 0)
                    {
                        moveDirection = Directions.Right;
                    }
                    else if (hor < 0)
                    {
                        moveDirection = Directions.Left;
                    }
                }
                
            }    
        }

        hor = 0;
        ver = 0;

        pItem.UseItem(pItem.holdingButton);
    }

    public void ReturnToStanding()
    {
        pState = PlayerState.Standing;
        SecondCheck();
    }

    public void MovePlayer(int direction)
    {
        //holdingButton = 0;
        Directions finalDirection = (Directions)direction;

        if (pState == PlayerState.Standing)
        {
            moveDirection = finalDirection;
            Orientate();            
        }
         
    }

    public void Orientate()
    {     
        switch (moveDirection)
        {
            case Directions.Front:
                {
                    print("Up");
                    SetDestination(new Vector3Int(playerX, playerY, playerZ + 1));
                    ChangeRotation(Quaternion.Euler(0, 0, 0));
                }
                break;

            case Directions.Back:
                {
                    print("Down");
                    if (transform.position.z - 1 >= 0)
                    {
                        print("Dá pra ir");
                        SetDestination(new Vector3Int(playerX, playerY, playerZ - 1));
                    }

                    ChangeRotation(Quaternion.Euler(0, 180, 0));
                }
                break;

            case Directions.Left:
                {
                    print("Left");
                    // Limitando movimento, para não sair do cenário²
                    {
                        if (transform.position.x - 1 >= 0)
                        {
                            print("Dá pra ir");
                            SetDestination(new Vector3Int(playerX - 1, playerY, playerZ));
                        }
                    }
                    ChangeRotation(Quaternion.Euler(0, -90, 0));
                }
                break;

            case Directions.Right:
                {
                    print("Right");
                    // Limitando movimento, para não sair do cenário³
                    {
                        if (transform.position.x + 1 <= lvlWidth)
                        {
                            print("Dá pra ir");
                            SetDestination(new Vector3Int(playerX + 1, playerY, playerZ));
                        }
                    }
                    ChangeRotation(Quaternion.Euler(0, 90, 0));
                }
                break;
        }
    }

    private void Update()
    {        
        if (holdingButton == true)
        {            
            if (pState == PlayerState.Standing)
            {
                MovePlayer((int)moveDirection);
            }
            
        }
        //GetPlayerPosition();
    }

    private void FixedUpdate()
    {
        print(controls.Miner.TouchStick.ReadValue<Vector2>());
        //GetPlayerPosition();

        if (pState != PlayerState.Standing)
        {
            switch (pState)
            {
                /*case PlayerState.Riding:
                    {
                        if (tmpMinecart != null)
                        {
                            if (tmpMinecart.moving == false)
                            {
                                EndRide();
                            }
                        }
                    }
                    break;*/

                case PlayerState.Falling:
                    {
                        transform.position = Vector3.MoveTowards(transform.position, destination, fallSpeed * Time.deltaTime);
                    }
                    break;

                case PlayerState.Walking:
                    {
                        transform.position = Vector3.MoveTowards(transform.position, destination, moveSpeed * Time.deltaTime);
                    }
                    break;

                case PlayerState.Jumping:
                    {
                        transform.position = Vector3.MoveTowards(transform.position, destination, moveSpeed * Time.deltaTime);
                    }
                    break;

                case PlayerState.Sliding:
                    {
                        transform.position = Vector3.MoveTowards(transform.position, destination, slideSpeed * Time.deltaTime);
                    }
                    break;
            }

            if (transform.position == destination)
            {
                transform.position = destination;
                if (pState != PlayerState.Riding && pState != PlayerState.Interacting)
                {
                    SecondCheck();
                }
            }
        }    
    }

    // Determina a posição para onde o personagem irá se mover
    public void SetDestination(Vector3Int destinationToSet, bool action = true)
    {
        GetPlayerPosition();

        // Determinando a posição e começando movimento
        destination = destinationToSet;

        if (action && pState != PlayerState.Interacting)
        {
            SetAction();
        }
    }

    // Muda a rotação do personagem
    public void ChangeRotation(Quaternion angle)
    {
        transform.localRotation = angle;
    }

    public bool FindObstacleList(ObstacleType[] type, int x, int y, int z, bool checkHeight)
    {
        if (lvlCoordinates.FindObstacleList(type, x, y, z, checkHeight))
        {
            return true;
        }
        return false;
    }

    public bool FindObstacle(ObstacleType type, int x, int y, int z, bool checkHeight)
    {
        if (lvlCoordinates.FindObstacle(type, x, y, z, checkHeight))
        {
            return true;
        }
        return false;
    }

    public bool FindCollectable(int x, int y, int z, bool collect = true)
    {

        if (lvlCoordinates.FindCollectable(x, y, z, collect))
        {
            tmpColType = lvlCoordinates.tmpColType;

            if (collect)
            {
                switch (tmpColType)
                {
                    case CollectableType.Diamond:
                        {
                            EmitParticle(diamondParticle, 5);
                            //diamondParticle.gameObject.transform.position = transform.position;
                            //diamondParticle.Emit(5);
                        }
                        break;

                    case CollectableType.Rubi:
                        {
                            EmitParticle(rubyParticle, 5);
                            //rubyParticle.gameObject.transform.position = transform.position;
                            //rubyParticle.Emit(5);
                        }
                        break;
                }
                itemAudio.PlayClip("Get");
            }            
            return true;
        }
        return false;
    }

    public GameObject ReturnObstacle(ObstacleType type, int x, int y, int z, bool checkHeight)
    {
        return lvlCoordinates.ReturnObstacle(type, x, y, z, true);       
    }

    // Busca os obstáculos após chegar no destino
    public void SecondCheck()
    {
        GetPlayerPosition();
        FindCollectable(playerX, playerY, playerZ);

        if (movingStone)
        {
            movingStone = false;
            //movingStoneSpeed = standartMovingStoneSpeed;
            tmpMovableStone = null;
            moveSpeed = standartSpeed;
            anim.speed = normalAnimSpeed;
        }

        if (pState == PlayerState.Jumping || pState == PlayerState.Falling)
        {
            if (playerY >= 1)
            {
                if (FindObstacleList(new ObstacleType[] { ObstacleType.Stone,
                    ObstacleType.EmeraldOre, ObstacleType.BrakedStone,
                    ObstacleType.MovableStone}, playerX, playerY - 1, playerZ, true))
                {
                    print("PARAR DE CAIR");
                    EndFall();
                }

                else
                {
                    if (!FindObstacleList(new ObstacleType[] { ObstacleType.Ladders }, playerX, playerY, playerZ, true))
                    {
                        print("CAIR");
                        Fall();
                        return;
                    }

                    else
                    {
                        anim.SetBool("Land", true);
                    }
                }
            }

            else
            {
                EndFall();
            }
        }

        if (pState != PlayerState.Riding)
        {
            if (FindObstacle(ObstacleType.MineCart, playerX, playerY, playerZ, true))
            {
                StartRide();
                return;
            }

            //else
            //{
            //    tmpMinecart = null;
            //}
        }

        if (FindObstacle(ObstacleType.Spikes, playerX, playerY, playerZ, true))
        {
            minerAudio.PlayClip("SpikeDeath");
            EmitParticle(spikeDeathParticles, 10);
            Die();
            return;
        }

        else if (FindObstacle(ObstacleType.Lava, playerX, playerY, playerZ, true))
        {
            if (pState != PlayerState.Riding)
            {
                LavaDeath();
                return;
            }
        }

        if (FindObstacle(ObstacleType.StageEnd, playerX, playerY, playerZ, true))
        {
            EndStage();
            return;
        }
     
        if (FindObstacle(ObstacleType.IceBlock, playerX, playerY - 1, playerZ, true))
        {
            if (!movingStone)
            {
                Slide();
                return;
            }

            else
            {
                EndSlide();
            }            
        }

        /*if (pState == PlayerState.MovingStone)
        {
            GetPlayerPosition();
            
        }*/

        if (pState == PlayerState.Sliding)
        {
            if (!FindObstacle(ObstacleType.IceBlock, playerX, playerY - 1, playerZ, true))
            {
                EndSlide();
            }                
        }

        if (hasMud)
        {
            hasMud = false;
            anim.speed = normalAnimSpeed;
            moveSpeed = standartSpeed;
        }

        if (FindObstacle(ObstacleType.Mud, playerX, playerY, playerZ, true))
        {
            hasMud = true;

            if (pState == PlayerState.Sliding)
            {
                EmitParticle(mudParticle, 5);

                minerAudio.PlayClip("MudStep");
                EndSlide();
            }
        }

        print("SecondCheck");
        if (pState != PlayerState.Interacting)
        {
            pState = PlayerState.Standing;
        }

            CheckInput();


    }

    // Define qual ação tomar, com base nos obstáculos
    public void SetAction()
    {        

        // MANIPULANDO OS BOOLEANS DE COLISÃO \\
        ResetCollisionBoolean();

        // Achando pedra a frente
        if (FindObstacleList(new ObstacleType[] { ObstacleType.Stone, ObstacleType.EmeraldOre,
            ObstacleType.BrakedStone }, destination.x, destination.y, destination.z, true))
        {
            hasStoneAhead = true;
        }

        // Achando pedra acima
        if (FindObstacleList(new ObstacleType[] { ObstacleType.Stone, ObstacleType.EmeraldOre,
            ObstacleType.BrakedStone, ObstacleType.MovableStone }, destination.x, destination.y + 1, destination.z, true))
        {
            hasStoneAbove = true;
        }

        // Achando pedra abaixo
        if (FindObstacleList(new ObstacleType[] { ObstacleType.Stone, ObstacleType.EmeraldOre,
            ObstacleType.BrakedStone, ObstacleType.MovableStone }, destination.x, destination.y - 1, destination.z, true))
        {
            hasStoneBelow = true;
        }

        if (FindObstacle(ObstacleType.Ladders, playerX, playerY, playerZ, true))
        {
            hasLadders = true;

            if (FindObstacle(ObstacleType.Ladders, playerX, playerY +1, playerZ, true))
            {
                hasLaddersAbove = true;
            }
        }

        else
        {
            canDetectLadders = true;
        }

        if (!hasStoneBelow)
        {
            if (playerY < 1)
            {
                hasStoneBelow = true;
            }
        }

        ////////////////////////////////////////////////
        ////////////////////////////////////////////////
        /////////////// EXECUTANDO AÇÃO ////////////////
        ////////////////////////////////////////////////
        ////////////////////////////////////////////////
        
        if (pState != PlayerState.Riding)
        {
            if (FindObstacle(ObstacleType.MineCart, playerX, playerY, playerZ, true))
            {
                if (hasStoneAhead && !hasStoneAbove)
                {
                    Climb(destination.x, destination.y + 1, destination.z);
                    return;
                }
                
                else if (!hasStoneAhead && !hasStoneAbove)
                {
                    Climb(destination.x, destination.y, destination.z);
                }
            }

            if (!hasStoneAbove)
            {
                if (FindObstacle(ObstacleType.MineCart, destination.x, destination.y, destination.z, true))
                {
                    Climb(destination.x, destination.y, destination.z);
                    return;
                }
            }
        }

        else
        {
            //ridingMinecart = false;
            if (FindObstacle(ObstacleType.MovableStone, destination.x, destination.y, destination.z, true))
            {
                hasStoneAhead = true;
            }

            if (!hasStoneAhead && !hasStoneAbove)
            {
                Climb(destination.x, destination.y, destination.z);
                
                return;
            }

            else
            {
                //CanMove(true);
                return;
            }
        }

        if (FindObstacle(ObstacleType.MovableStone, destination.x, destination.y, destination.z, true))
        {
            //if (ridingMinecart)
            //{
            //    hasStoneAhead = true;
            //}

            //else
            //{
                tmpMovableStone = ReturnObstacle(ObstacleType.MovableStone, destination.x, destination.y, destination.z, true).GetComponent<MovableStone>();
                if (tmpMovableStone.sliding)
                {
                    hasStoneAhead = true;
                    return;
                }
                tmpMovableStone.GetDirection(moveDirection);
                tmpMovableStone.SetAction();

                if (tmpMovableStone.canPush)
                {
                    MoveStone();
                    return;
                }

                else
                {
                    hasStoneAhead = true;
                }
            //}
            
        }

        if (FindObstacle(ObstacleType.IceBlock, playerX, playerY - 1, playerZ, true))
        {
            if (!hasStoneAhead)
            {
                Slide();
                return;
            }            
        }

        //else
        //{
        //    tmpMovableStone = null;
        //}

        if (hasLadders)
        {
            // Identifica qual a direção certa da escada, pra poder definir a posição contrária e descê-la
            if (canDetectLadders)
            {
                if (hasStoneAhead)
                {
                    ladderDirection = moveDirection;

                    canDetectLadders = false;
                }
            }

            // Sobe caso a direção pressionada seja a certa
            if (moveDirection == ladderDirection)
            {
                // Se tem escada acima, apenas sobe
                if (hasLaddersAbove)
                {
                    Walk(playerX, playerY + 1, playerZ);
                }

                // Se não tem, sobe na pedra diretamente
                else
                {
                    Climb(destination.x, destination.y + 1, destination.z);
                }
                return;
            }

            // Descer escada ao pressionar posição contrária
            else if ((int)moveDirection == -(int)ladderDirection)
            {
                if (!hasStoneBelow)
                {
                    Walk(playerX, playerY - 1, playerZ);
                    return;
                }
            }

            // Pulando caso pressione outra posição
            else
            {
                if (playerY > 0)
                {
                    Climb(destination.x, destination.y - 1, destination.z);
                    return;
                }                
            }
        }

        if (hasStoneAhead)
        {
            if (!hasStoneAbove)
            {
                Climb(destination.x, destination.y + 1, destination.z);
                return;
            }
        }

        else
        {            
            if (hasStoneBelow)
            {
                Walk(destination.x, destination.y, destination.z);
            }

            else
            {
                if (FindObstacle(ObstacleType.Ladders, destination.x, destination.y- 1, destination.z, true))
                {
                    Climb(destination.x, destination.y - 1, destination.z);

                    int d = -(int)moveDirection;

                    ladderDirection = (Directions)d;
                    canDetectLadders = false;
                    return;
                }

                if (playerY == 1)
                {
                        Climb(destination.x, destination.y - 1, destination.z);                    
                }

                else
                {
                        Climb(destination.x, destination.y - 1, destination.z);
                }
                print("Não tem nada, descer");
            }
        }
    }

    public void ResetCollisionBoolean()
    {
        hasStoneAhead = false;
        hasStoneAbove = false;
        hasStoneBelow = false;

        hasLadders = false;
        hasLaddersAbove = false;
    }

    [ContextMenu ("GetPlayerPosition")]
    public void GetPlayerPosition()
    {
        playerX = (int)transform.position.x;
        playerY = (int)transform.position.y;
        playerZ = (int)transform.position.z;
    }

    public void Walk(int x, int y, int z)
    {
        pState = PlayerState.Walking;
        if (movingStone)
        {
            if (hasMud)
            {
                moveSpeed = mudSpeed;
                anim.speed = slowAnimSpeed;
                movingStoneSpeed = mudSpeed;
            }

            else
            {
                anim.speed = slowAnimSpeed * 2;
                moveSpeed = movingStoneSpeed;
            }  
        }

        else if (hasMud)
        {
            minerAudio.PlayClip("MudStep");
            anim.speed = slowAnimSpeed;
            moveSpeed = mudSpeed;
            EmitParticle(mudParticle, 5);
        }


        else
        {
            if (hasLadders && hasStoneAhead)
            {
                minerAudio.PlayClip("WoodStep");
            }

            else
            {
                minerAudio.PlayClip("StoneStep");
            }
        }

        destination = new Vector3Int(x, y, z);

        StepPlay();

        if (FindObstacle(ObstacleType.IceBlock, playerX, playerY - 1, playerZ, true))
        {
            if(!FindObstacle(ObstacleType.MovableStone, destination.x, destination.y, destination.z, true))
            {
                Slide();
                return;
            }
        }

        //isMoving = true;
    }

    public void StepPlay()
    {
        rightStep = !rightStep;
        anim.SetBool("RightStep", rightStep);
        anim.SetTrigger("Walk");

    }

    public void Climb(int x, int y, int z)
    {
        if (hasMud)
        {
            anim.speed = slowAnimSpeed * 2;
            moveSpeed = mudSpeed * 3;
            EmitParticle(mudParticle, 5);
        }

        minerAudio.PlayClip("Jump");

        destination = new Vector3Int(x, y, z);

        anim.SetBool("Land", false);
        anim.SetTrigger("Climb");

        //isMoving = true;
        //jumping = true;

        pState = PlayerState.Jumping;
    }

    public void Slide()
    {
        pState = PlayerState.Sliding;
        //iceSliding = true;
        //canMove = false;

        switch (moveDirection)
        {
            case Directions.Front:
                {
                    SetDestination(new Vector3Int(playerX, playerY, playerZ + 1), false);
                }
                break;

            case Directions.Back:
                {
                    SetDestination(new Vector3Int(playerX, playerY, playerZ - 1), false);
                    if (destination.y < 0)
                    {
                        EndSlide();
                        return;
                    }
                }
                break;

            case Directions.Left:
                {
                    SetDestination(new Vector3Int(playerX - 1, playerY, playerZ), false);
                    if (destination.x < 0)
                    {
                        EndSlide();
                        return;
                    }
                }
                break;

            case Directions.Right:
                {
                    SetDestination(new Vector3Int(playerX + 1, playerY, playerZ), false);
                    if (destination.x > lvlWidth)
                    {
                        EndSlide();
                        return;
                    }
                }
                break;
        }

        if (FindObstacleList(new ObstacleType[] { ObstacleType.Stone, ObstacleType.EmeraldOre,
        ObstacleType.BrakedStone, ObstacleType.MovableStone}, destination.x, destination.y, destination.z, true))
        {
            minerAudio.PlayClip("HitWall");
            EndSlide();
        }

        else
        {
            moveSpeed = slideSpeed;
            anim.SetBool("Sliding", true);
            minerAudio.PlayClip("IceSlip");
            //isMoving = true;
        }        
    }

    public void EndSlide()
    {
        print("EndSlide");
        pState = PlayerState.Standing;
        //CanMove(true);
        //iceSliding = false;
        anim.SetBool("Sliding", false);
        //isMoving = false;
        moveSpeed = standartSpeed;
    }

    public void Fall()
    {
        //canMove = false;
        SetDestination(new Vector3Int(playerX, playerY - 1, playerZ), false);
        print("Destino da queda " + destination);
        pState = PlayerState.Falling;
        //falling = true;
        //isMoving = true;
    }

    public void EndFall()
    {
        if (!minerAudio.audioSource.isPlaying)
        {
            minerAudio.PlayClip("HitWall");
        }        
        anim.SetBool("Land", true);
        //jumping = false;
        //falling = false;
        //isMoving = false;
        //CanMove(true);
        pState = PlayerState.Standing;
        print("Acabou a queda");
    }

    public void MoveStone()
    {
        movingStone = true;
        tmpMovableStone.StartMove();
        tmpMovableStone.GetSpeed(movingStoneSpeed);
        Walk(destination.x,destination.y,destination.z);
    }

    public void StartRide()
    {
        anim.SetBool("Riding", true);
        pState = PlayerState.Riding;
        print(ReturnObstacle(ObstacleType.MineCart, playerX, playerY, playerZ, true));
        tmpMinecart = ReturnObstacle(ObstacleType.MineCart, playerX, playerY, playerZ, true).GetComponent<Minecart>();
        tmpMinecart.GetPlayer();
        tmpMinecart.SetNextMove();
        tmpMinecart.enabled = true;
        lvlCoordinates.RemoveUnit(tmpMinecart.gameObject, tmpMinecart.x, tmpMinecart.z);
        transform.parent = tmpMinecart.transform;
        transform.rotation = tmpMinecart.transform.rotation;
        //enabled = false;
    }

    public void EndRide()
    {
        //enabled = true;
        anim.SetBool("Riding", false);
        moveDirection = tmpMinecart.currentDirection;
        transform.parent = originalParent;
        GetPlayerPosition();
        switch (moveDirection)
        {
            case Directions.Front:
                {
                    destination = new Vector3Int(playerX, playerY, playerZ + 1);
                }
                break;

            case Directions.Back:
                {
                    if (playerZ - 1 < 0)
                    {
                        tmpMinecart.Inverse();
                        //lvlCoordinates.AddToMatrix(tmpMinecart.gameObject, UnitType.Obstacle);
                        SecondCheck();
                        return;
                    }
                    destination = new Vector3Int(playerX, playerY, playerZ - 1);
                }
                break;

            case Directions.Left:
                {
                    if (playerX - 1 < 0)
                    {
                        tmpMinecart.Inverse();
                        //lvlCoordinates.AddToMatrix(tmpMinecart.gameObject, UnitType.Obstacle);
                        SecondCheck();
                        return;
                    }
                    destination = new Vector3Int(playerX - 1, playerY, playerZ);
                }
                break;

            case Directions.Right:
                {
                    if (playerX + 1 > lvlWidth)
                    {
                        tmpMinecart.Inverse();
                        //lvlCoordinates.AddToMatrix(tmpMinecart.gameObject, UnitType.Obstacle);
                        SecondCheck();
                        return;
                    }

                    destination = new Vector3Int(playerX + 1, playerY, playerZ);
                }
                break;
        }
        SetAction();
        tmpMinecart.Inverse();        
        lvlCoordinates.AddToMatrix(tmpMinecart.gameObject, UnitType.Obstacle);
        tmpMinecart = null;
    }

    public void Die()
    {
        if (enabled)
        {
            FindObjectOfType<StagePersistent>().AddDeath();
            if (pState == PlayerState.Riding)
            {
                if (tmpMinecart != null)
                {
                    tmpMinecart.moving = false;
                }                
                //ridingMinecart = false;
            }
            anim.speed = normalAnimSpeed;
            anim.SetTrigger("Die");
            minerVoice.PlayClip("Grunhido" + Random.Range(1,4));
            StartCoroutine(FadeCoroutine(false));
            this.enabled = false;
        }
    }

    public void LavaDeath()
    {
        if (enabled)
        {
            if (pState == PlayerState.Riding)
            {
                if (tmpMinecart != null)
                {
                    tmpMinecart.moving = false;
                }
                //ridingMinecart = false;
            }

            EmitParticle(lavaDeathParticles, 30);
            minerAudio.PlayClip("LavaDeath");
            Die();
        }        
    }

    public void GetPickaxe()
    {
        pItem.GetPickaxe();
        //hasPickaxe = true;
        anim.SetTrigger("Pickaxe");
        minerVoice.PlayClip("Yay");
        pState = PlayerState.Interacting;
        //CanMove(false);
    }        

    public void EmitParticle(ParticleSystem pSys, int amount)
    {
        pSys.gameObject.transform.position = transform.position;
        pSys.Emit(amount);
    }
    //public void CanMove(bool value)
    //{
    //    canMove = value;
    //    enabled = value;
    //}


    [ContextMenu("EndStage")]
    public void EndStage()
    {
        minerVoice.PlayClip("YuHu");
        StartCoroutine(FadeCoroutine(true));
        this.enabled = false;
    }

    public IEnumerator FadeCoroutine(bool completed)
    {
        yield return new WaitForSeconds(1.5f);

        if (completed)
        {
            endStageEvent.Invoke();
        }

        else
        {
            deathEvent.Invoke();
        }
    }
}
