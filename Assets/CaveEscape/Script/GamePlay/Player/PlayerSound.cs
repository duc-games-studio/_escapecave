using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSound : MonoBehaviour
{
    ///// AUDIO \\\\\
    public SFX minerAudio;
    public SFX minerVoice;
    public SFX itemAudio;

}
