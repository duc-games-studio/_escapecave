﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// SCRIPTABLE OBJECT QUE CONTÉM LISTA DE OBJETOS A SEREM USADOS NO OBJECT PLACER
// ÚTIL PARA TEMÁTCIAS DIFERENTES
[CreateAssetMenu(fileName = "New Container", menuName = "ObjectContainer")]
public class ObjectsContainer : ScriptableObject
{
    ///// CLASSE \\\\\

    // Classe de lista que guardas os objetos a serem usados

    // Diz que é possível editar as variáveis dentro da classe no Unity
    [System.Serializable]
    public class ObstacleList
    {
        // Tipo de objeto
        public ObstacleType theType;

        // O Objeto (prefabs)
        public GameObject[] theObstacle;
    }

    [System.Serializable]
    public class CollectableList
    {
        // Tipo de coletável
        public CollectableType collectableType;

        // Objetos (Prefabs)
        public GameObject[] theCollectable;
    }

    [System.Serializable]
    public class TrailList
    {
        // Tipo de coletável
        public TrailType trailType;

        // Objetos (Prefabs)
        public GameObject theTrail;
    }

    // Lista de objetos da classe "ObstacleList"
    public ObstacleList[] obstacleList;

    // Lista de objetos da classe "CollectableList"
    public CollectableList[] collectableList;

    // Lista de objetos da classe "TrailList"
    public TrailList[] trailList;
}
