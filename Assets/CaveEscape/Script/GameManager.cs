﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    // Singleton

    public static GameManager instance;

    public void Awake()
    {
        if (!instance)
        {
            instance = this;
        }

        else
        {
            Destroy(gameObject);
        }
    }

    [Header("Itens de administração")]
    [Header("Evita múltiplos singleton")]

    [Header("Guarda posição de obstáculos")]
    public LevelCoordinates lvlCoordinates;

    [Header("Guarda eventos a serem executados na coleta de objetos")]
    public CollectableEvents colEvents;

    [Header("Guarda pontuação do jogador")]
    public Score score;

    [Header("Administra visuais da HUD")]
    public HUDManager hudManager;

    [Header("Guarda pequenas informações da fase, faz carregamento")]
    public StageInfo stageInfo;

}
