// GENERATED AUTOMATICALLY FROM 'Assets/Input/PlayerControls.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

public class @PlayerControls : IInputActionCollection, IDisposable
{
    public InputActionAsset asset { get; }
    public @PlayerControls()
    {
        asset = InputActionAsset.FromJson(@"{
    ""name"": ""PlayerControls"",
    ""maps"": [
        {
            ""name"": ""Miner"",
            ""id"": ""5b727e81-d04a-42f5-afa8-24393a886d28"",
            ""actions"": [
                {
                    ""name"": ""MinerUp"",
                    ""type"": ""Button"",
                    ""id"": ""7f274859-5b78-4ce9-9bca-93ccedb4e90a"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""MinerDown"",
                    ""type"": ""Button"",
                    ""id"": ""28ec0a3d-5c20-4afe-953c-5b72462f4b2c"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""MinerLeft"",
                    ""type"": ""Button"",
                    ""id"": ""3332df39-de08-47d0-ad08-033b829d79b7"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""MinerRight"",
                    ""type"": ""Button"",
                    ""id"": ""25418031-c2e4-4987-bb3b-d48bdd96b0e2"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""MinerItem"",
                    ""type"": ""Button"",
                    ""id"": ""570b2838-3a93-4139-b1b9-db1dac527d42"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Pause"",
                    ""type"": ""Button"",
                    ""id"": ""a7bf5180-e2bc-496f-a6e9-9182fee849b6"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""MinerMovement"",
                    ""type"": ""PassThrough"",
                    ""id"": ""4534dc79-bf63-4bbb-b645-c60bcde32463"",
                    ""expectedControlType"": ""Vector2"",
                    ""processors"": ""ScaleVector2"",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Touch"",
                    ""type"": ""Button"",
                    ""id"": ""4fea21b6-033c-413b-891a-607dc6271dca"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""TouchStick"",
                    ""type"": ""Value"",
                    ""id"": ""07489446-1aeb-4ef2-9deb-2da5c1dea1c6"",
                    ""expectedControlType"": ""Vector2"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""e6473706-cdbd-4e96-9389-c30f4f198bc1"",
                    ""path"": ""<Keyboard>/w"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""MinerUp"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""45bc4897-9d11-4ce9-8092-b50440cbdb59"",
                    ""path"": ""<Keyboard>/upArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""MinerUp"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""58f355ef-7302-44e1-b87b-15ebe3f40057"",
                    ""path"": ""<Keyboard>/s"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""MinerDown"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""3bdbb9a7-f934-4685-9f53-bfb797088265"",
                    ""path"": ""<Keyboard>/downArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""MinerDown"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""41cbbc9c-90bf-4f54-941f-5f067ffa5441"",
                    ""path"": ""<Keyboard>/a"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""MinerLeft"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""8abf4758-00e1-4d7c-941d-d7298f5f92c3"",
                    ""path"": ""<Keyboard>/leftArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""MinerLeft"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""7207f90b-28d3-4878-afe8-f7d050f1bf01"",
                    ""path"": ""<Keyboard>/d"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""MinerRight"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""623bb7d9-5354-4f8e-b25a-cc0d346a6e16"",
                    ""path"": ""<Keyboard>/rightArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""MinerRight"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""3bb2271b-f830-4314-8860-fc236d848a53"",
                    ""path"": ""<Keyboard>/space"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""MinerItem"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""51dbdedd-0388-4e49-9ed9-9f535d5aee3c"",
                    ""path"": ""<Keyboard>/p"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Pause"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""e323e9c2-99fa-446f-bb52-5c686ac5adfa"",
                    ""path"": ""<Keyboard>/escape"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Pause"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""WASD"",
                    ""id"": ""ff98953e-6b53-42dd-8200-e998f48383fb"",
                    ""path"": ""2DVector"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""MinerMovement"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""up"",
                    ""id"": ""3ff4763c-df2f-4286-a081-a77e97dae6ab"",
                    ""path"": ""<Keyboard>/w"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""MinerMovement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""down"",
                    ""id"": ""d7a40443-9447-422a-9bc1-bfc2a49be52b"",
                    ""path"": ""<Keyboard>/s"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""MinerMovement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""left"",
                    ""id"": ""5ed3a0e6-0670-401a-b0e5-624da919eb18"",
                    ""path"": ""<Keyboard>/a"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""MinerMovement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""right"",
                    ""id"": ""a4667798-f747-4e3f-8bac-70745fddd856"",
                    ""path"": ""<Keyboard>/d"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""MinerMovement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""Arrows"",
                    ""id"": ""93904a41-7a51-4dcd-9254-8355435bfa16"",
                    ""path"": ""2DVector"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""MinerMovement"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""up"",
                    ""id"": ""1fb89e52-929f-40f9-b5fd-dc6938450cbd"",
                    ""path"": ""<Keyboard>/upArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""MinerMovement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""down"",
                    ""id"": ""c0ec544c-2e64-4831-9891-75bd667a8b0a"",
                    ""path"": ""<Keyboard>/downArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""MinerMovement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""left"",
                    ""id"": ""bb855d55-4adb-4090-9192-f094b5d7c989"",
                    ""path"": ""<Keyboard>/leftArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""MinerMovement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""right"",
                    ""id"": ""83ebc997-2dfb-42d7-a388-f6c7e2eada35"",
                    ""path"": ""<Keyboard>/rightArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""MinerMovement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": """",
                    ""id"": ""90d5fa90-eb69-4c44-aa60-17fe2602b62e"",
                    ""path"": ""<Touchscreen>/press"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Touch"",
                    ""action"": ""Touch"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""ff606283-5149-4c24-b0e0-c7320fc368c1"",
                    ""path"": ""<Gamepad>/leftStick"",
                    ""interactions"": """",
                    ""processors"": ""StickDeadzone"",
                    ""groups"": """",
                    ""action"": ""TouchStick"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""66d90f3c-c8d6-4727-855e-46d1ae251850"",
                    ""path"": ""<AndroidJoystick>/stick"",
                    ""interactions"": """",
                    ""processors"": ""StickDeadzone"",
                    ""groups"": """",
                    ""action"": ""TouchStick"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": [
        {
            ""name"": ""Touch"",
            ""bindingGroup"": ""Touch"",
            ""devices"": [
                {
                    ""devicePath"": ""<Touchscreen>"",
                    ""isOptional"": false,
                    ""isOR"": false
                }
            ]
        }
    ]
}");
        // Miner
        m_Miner = asset.FindActionMap("Miner", throwIfNotFound: true);
        m_Miner_MinerUp = m_Miner.FindAction("MinerUp", throwIfNotFound: true);
        m_Miner_MinerDown = m_Miner.FindAction("MinerDown", throwIfNotFound: true);
        m_Miner_MinerLeft = m_Miner.FindAction("MinerLeft", throwIfNotFound: true);
        m_Miner_MinerRight = m_Miner.FindAction("MinerRight", throwIfNotFound: true);
        m_Miner_MinerItem = m_Miner.FindAction("MinerItem", throwIfNotFound: true);
        m_Miner_Pause = m_Miner.FindAction("Pause", throwIfNotFound: true);
        m_Miner_MinerMovement = m_Miner.FindAction("MinerMovement", throwIfNotFound: true);
        m_Miner_Touch = m_Miner.FindAction("Touch", throwIfNotFound: true);
        m_Miner_TouchStick = m_Miner.FindAction("TouchStick", throwIfNotFound: true);
    }

    public void Dispose()
    {
        UnityEngine.Object.Destroy(asset);
    }

    public InputBinding? bindingMask
    {
        get => asset.bindingMask;
        set => asset.bindingMask = value;
    }

    public ReadOnlyArray<InputDevice>? devices
    {
        get => asset.devices;
        set => asset.devices = value;
    }

    public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

    public bool Contains(InputAction action)
    {
        return asset.Contains(action);
    }

    public IEnumerator<InputAction> GetEnumerator()
    {
        return asset.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public void Enable()
    {
        asset.Enable();
    }

    public void Disable()
    {
        asset.Disable();
    }

    // Miner
    private readonly InputActionMap m_Miner;
    private IMinerActions m_MinerActionsCallbackInterface;
    private readonly InputAction m_Miner_MinerUp;
    private readonly InputAction m_Miner_MinerDown;
    private readonly InputAction m_Miner_MinerLeft;
    private readonly InputAction m_Miner_MinerRight;
    private readonly InputAction m_Miner_MinerItem;
    private readonly InputAction m_Miner_Pause;
    private readonly InputAction m_Miner_MinerMovement;
    private readonly InputAction m_Miner_Touch;
    private readonly InputAction m_Miner_TouchStick;
    public struct MinerActions
    {
        private @PlayerControls m_Wrapper;
        public MinerActions(@PlayerControls wrapper) { m_Wrapper = wrapper; }
        public InputAction @MinerUp => m_Wrapper.m_Miner_MinerUp;
        public InputAction @MinerDown => m_Wrapper.m_Miner_MinerDown;
        public InputAction @MinerLeft => m_Wrapper.m_Miner_MinerLeft;
        public InputAction @MinerRight => m_Wrapper.m_Miner_MinerRight;
        public InputAction @MinerItem => m_Wrapper.m_Miner_MinerItem;
        public InputAction @Pause => m_Wrapper.m_Miner_Pause;
        public InputAction @MinerMovement => m_Wrapper.m_Miner_MinerMovement;
        public InputAction @Touch => m_Wrapper.m_Miner_Touch;
        public InputAction @TouchStick => m_Wrapper.m_Miner_TouchStick;
        public InputActionMap Get() { return m_Wrapper.m_Miner; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(MinerActions set) { return set.Get(); }
        public void SetCallbacks(IMinerActions instance)
        {
            if (m_Wrapper.m_MinerActionsCallbackInterface != null)
            {
                @MinerUp.started -= m_Wrapper.m_MinerActionsCallbackInterface.OnMinerUp;
                @MinerUp.performed -= m_Wrapper.m_MinerActionsCallbackInterface.OnMinerUp;
                @MinerUp.canceled -= m_Wrapper.m_MinerActionsCallbackInterface.OnMinerUp;
                @MinerDown.started -= m_Wrapper.m_MinerActionsCallbackInterface.OnMinerDown;
                @MinerDown.performed -= m_Wrapper.m_MinerActionsCallbackInterface.OnMinerDown;
                @MinerDown.canceled -= m_Wrapper.m_MinerActionsCallbackInterface.OnMinerDown;
                @MinerLeft.started -= m_Wrapper.m_MinerActionsCallbackInterface.OnMinerLeft;
                @MinerLeft.performed -= m_Wrapper.m_MinerActionsCallbackInterface.OnMinerLeft;
                @MinerLeft.canceled -= m_Wrapper.m_MinerActionsCallbackInterface.OnMinerLeft;
                @MinerRight.started -= m_Wrapper.m_MinerActionsCallbackInterface.OnMinerRight;
                @MinerRight.performed -= m_Wrapper.m_MinerActionsCallbackInterface.OnMinerRight;
                @MinerRight.canceled -= m_Wrapper.m_MinerActionsCallbackInterface.OnMinerRight;
                @MinerItem.started -= m_Wrapper.m_MinerActionsCallbackInterface.OnMinerItem;
                @MinerItem.performed -= m_Wrapper.m_MinerActionsCallbackInterface.OnMinerItem;
                @MinerItem.canceled -= m_Wrapper.m_MinerActionsCallbackInterface.OnMinerItem;
                @Pause.started -= m_Wrapper.m_MinerActionsCallbackInterface.OnPause;
                @Pause.performed -= m_Wrapper.m_MinerActionsCallbackInterface.OnPause;
                @Pause.canceled -= m_Wrapper.m_MinerActionsCallbackInterface.OnPause;
                @MinerMovement.started -= m_Wrapper.m_MinerActionsCallbackInterface.OnMinerMovement;
                @MinerMovement.performed -= m_Wrapper.m_MinerActionsCallbackInterface.OnMinerMovement;
                @MinerMovement.canceled -= m_Wrapper.m_MinerActionsCallbackInterface.OnMinerMovement;
                @Touch.started -= m_Wrapper.m_MinerActionsCallbackInterface.OnTouch;
                @Touch.performed -= m_Wrapper.m_MinerActionsCallbackInterface.OnTouch;
                @Touch.canceled -= m_Wrapper.m_MinerActionsCallbackInterface.OnTouch;
                @TouchStick.started -= m_Wrapper.m_MinerActionsCallbackInterface.OnTouchStick;
                @TouchStick.performed -= m_Wrapper.m_MinerActionsCallbackInterface.OnTouchStick;
                @TouchStick.canceled -= m_Wrapper.m_MinerActionsCallbackInterface.OnTouchStick;
            }
            m_Wrapper.m_MinerActionsCallbackInterface = instance;
            if (instance != null)
            {
                @MinerUp.started += instance.OnMinerUp;
                @MinerUp.performed += instance.OnMinerUp;
                @MinerUp.canceled += instance.OnMinerUp;
                @MinerDown.started += instance.OnMinerDown;
                @MinerDown.performed += instance.OnMinerDown;
                @MinerDown.canceled += instance.OnMinerDown;
                @MinerLeft.started += instance.OnMinerLeft;
                @MinerLeft.performed += instance.OnMinerLeft;
                @MinerLeft.canceled += instance.OnMinerLeft;
                @MinerRight.started += instance.OnMinerRight;
                @MinerRight.performed += instance.OnMinerRight;
                @MinerRight.canceled += instance.OnMinerRight;
                @MinerItem.started += instance.OnMinerItem;
                @MinerItem.performed += instance.OnMinerItem;
                @MinerItem.canceled += instance.OnMinerItem;
                @Pause.started += instance.OnPause;
                @Pause.performed += instance.OnPause;
                @Pause.canceled += instance.OnPause;
                @MinerMovement.started += instance.OnMinerMovement;
                @MinerMovement.performed += instance.OnMinerMovement;
                @MinerMovement.canceled += instance.OnMinerMovement;
                @Touch.started += instance.OnTouch;
                @Touch.performed += instance.OnTouch;
                @Touch.canceled += instance.OnTouch;
                @TouchStick.started += instance.OnTouchStick;
                @TouchStick.performed += instance.OnTouchStick;
                @TouchStick.canceled += instance.OnTouchStick;
            }
        }
    }
    public MinerActions @Miner => new MinerActions(this);
    private int m_TouchSchemeIndex = -1;
    public InputControlScheme TouchScheme
    {
        get
        {
            if (m_TouchSchemeIndex == -1) m_TouchSchemeIndex = asset.FindControlSchemeIndex("Touch");
            return asset.controlSchemes[m_TouchSchemeIndex];
        }
    }
    public interface IMinerActions
    {
        void OnMinerUp(InputAction.CallbackContext context);
        void OnMinerDown(InputAction.CallbackContext context);
        void OnMinerLeft(InputAction.CallbackContext context);
        void OnMinerRight(InputAction.CallbackContext context);
        void OnMinerItem(InputAction.CallbackContext context);
        void OnPause(InputAction.CallbackContext context);
        void OnMinerMovement(InputAction.CallbackContext context);
        void OnTouch(InputAction.CallbackContext context);
        void OnTouchStick(InputAction.CallbackContext context);
    }
}
